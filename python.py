# -*- coding: utf-8 -*-
import sys
from pyswip import Prolog

def assertAll(list,list_2):
	i = 0
	p = Prolog()

	list = list.splitlines()
	for item in list:
		# print item
		p.assertz(item.replace(" ", ""))

	list_2 = list_2.splitlines()
	for item2 in list_2:
		# print(item2)
		p.assertz(item2.replace(" ", ""))

	p.assertz("isi(X, Y, Z) :- terdiriDari(judul, [X,Y,Z]), jenis(X), nomor(Y), tahun(Z)")
	p.assertz("cekLink(X,Y,Z) :- idItemPasal(X), linkKonsiderans(Y, X), memiliki(itemPasal, [X, _, Z]), jenis(Y,Z)")

	try:
		for pquery in p.query("cekLink(X,Y,Z)"):
			i += 1
			# print(pquery)
			# print pquery["X"] + " believes " + pquery["Y"]+ " believes " + pquery["Z"]

		if i >=	1:
			# print "true" + "iterasi" + str(i)
			print('true')
		else:
			# print "false" + "iterasi" + str(i)
			print('false')
	except:
		print("false")

try:
	
	file = open("../storage/app/"+sys.argv[1]+".pl", "r")
	list2 = file.read()
	file.close

	file_2 = open("../storage/app/"+sys.argv[2]+".pl", "r")
	list2_2 = file_2.read()
	file_2.close

	assertAll(list2,list2_2)
except IOError:
	print "file not found"