## Instalasi

 1. Copy file .env.example menjadi .env 
 `cp .env.example .env`
 2. `composer install`
 3. `php artisan migrate`

### Database
##### Mongodb
 1. install mongodb 3.4
`sudo apt-get install mongodb`
 2. install php extension untuk mongodb 
`pecl install mongodb`
 3. create user untuk mongodb
```
use admindb.createUser(
   {
     user: "username",
     pwd: "password",
     roles: ["root"]
   }
)
```
 4. buka file /etc/mongodb.conf dan tambahkan line berikut
`auth = true`

###### BaseX
 5. Install BaseX
`sudo apt-get install basex`
 6. Jalankan basex server 
`basexserver -S`
 7. masuk ke dalam basex client
`basexclient`
 8. berikut adalah akun default
`username : admin`
`password : admin`
 9. Buat user untuk BaseX dan masukkan password saat diminta
`CREATE USER username`
 10. grant user ke database 
`GRANT write ON database TO username`
 11. buat database baru
`create db legal_draft`

### xml editor
#### Xonomy JS

 [http://www.lexiconista.com/xonomy/](http://www.lexiconista.com/xonomy/)

 1. Download dan ekstrak di [legal_drafter]/public/plugins/
 2. include file xonomy.js dan xonomy.css di halaman html.
 3. tambahkan script js berikut pada html
 ```
 <script>
 function start(){ 
	 var xml=""; 
	 var editor=document.getElementById("editor"); 
	 Xonomy.render(xml, editor, null); 
 }
 </script>
 ```
 
### Link Diagram
#### sigma.js
 http://sigmajs.org/
1. Download dan ekstrak di [legal_drafter]/public/plugins/
2. include file
3.  Tambahkan perintah berikut untuk menampilkan graph
```
<script>
sigma.parsers.json('data.json', { 
	container: 'container', 
	settings: { 
		defaultNodeColor: '#ec5148' 
	} 
});
</script>
```

## How To
### menyimpan file xml ke basex menggunakan php

```
$session = new Session_Basex(env('BASEX_HOST', 'localhost'), env('BASEX_PORT', '1984'), env('BASEX_USERNAME', 'admin'), env('BASEX_PASSWORD', 'admin'));

$session->execute("open legal_draft");

$save = $session->replace("/drafts/".$nama_xml.".xml", $xml_string);
$this->convert_to_pl($xml_string,$nama_xml);
$session->close();
```

### mengambil file xml dari basex menggunakan php

```
$session = new Session_Basex(env('BASEX_HOST', 'localhost'), env('BASEX_PORT', '1984'), env('BASEX_USERNAME', 'admin'), env('BASEX_PASSWORD', 'admin'));

$data['xml'] = trim(preg_replace('/\s\s+/', '', str_replace("\n", "", $session->execute("xquery db:open('legal_draft','/drafts/".$data['draft']->nama_xml.".xml')"))));

// xml string to xml element object
$xml=simplexml_load_string($data['xml']) or die("Error: Cannot create object");
```

### konversi xml to pdf (menggunakan xslt)
```
$session = new Session_Basex(env('BASEX_HOST', 'localhost'), env('BASEX_PORT', '1984'), env('BASEX_USERNAME', 'admin'), env('BASEX_PASSWORD', 'admin'));

// load dokumen XML
$data['xml'] = trim(preg_replace('/\s\s+/', '', str_replace("\n", "", $session->execute("xquery db:open('legal_draft','/drafts/".$data['draft']->nama_xml.".xml')"))));
$xml_doc = new DOMDocument();
$xml_doc->loadXML($data['xml']);

// Load dokumen XSL
$xsl_doc = new DOMDocument();
$xsl_doc->load('../resources/views/admin/builder/legal.xsl');

// XSLT Processor, xml dan xsl digabung
$proc = new XSLTProcessor();
$proc->importStylesheet($xsl_doc);
$newdom = $proc->transformToDoc($xml_doc);

//render pdf menggunakan DomPDF
$pdf = PDF::loadHTML($newdom->saveXML())->setPaper(array(0,0,612,972), 'portrait');

// streamin file pdf ke user
return $pdf->stream('tes.pdf');
```

### konversi XML to PL

```
// xml string to xml element object
$xml = new SimpleXMLElement($xml_string);

$str_temp = '';
$jd = $this->jenis_dokumen($xml->judul->jenis);

// generate data judul
$str_temp .= "%jenis(".$jd."). \n%nomor(".$xml->judul->nomor."). \n%tahun(".$xml->judul->tahun."). \nidJudul('".$xml->judul->attributes()->idJudul."').\njenis('".$xml->judul->attributes()->idJudul."', ".$jd.").\nnomor('".$xml->judul->attributes()->idJudul."', ".$xml->judul->nomor.").\ntahun('".$xml->judul->attributes()->idJudul."', ".$xml->judul->tahun.").\nkodifikasi('".$xml->judul->attributes()->idJudul."', ".$xml->judul->attributes()->kodifikasi.").\nkompilasi('".$xml->judul->attributes()->idJudul."', ".$xml->judul->attributes()->kompilasi.").\nidJabatan('".$xml->judul->attributes()->idJudul."', '".$xml->jabatanPembentuk->attributes()->idJabatan."').\n";

//perulangan untuk element xml yang ada lebih dari satu
foreach($xml->children()-> as $k => $v)
{
  // element to rule pl
}

// simpan ke dalam file dengan format .pl
Storage::disk('local')->put($filename.'.pl', $str_temp);
```