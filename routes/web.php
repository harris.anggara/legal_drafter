<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::any('/builder', 'BuilderController@index')->name('builder');
Route::post('/builder_post', 'BuilderController@simpan')->name('builder_post');
Route::post('/builder-preview', 'BuilderController@preview')->name('preview');
Route::post('/build-pdf', 'BuilderController@build_pdf')->name('build_pdf');
Route::get('/list-draft', 'BuilderController@list_draft')->name('list_draft');
Route::get('/list-draft-dt', 'BuilderController@dt_list_draft')->name('dt_list_draft');
Route::post('/hapus', 'BuilderController@hapus')->name('hapus');

Route::any('/anotator', 'BuilderController@anotator')->name('anotator');
Route::post('/upload_pdf_anotator', 'BuilderController@upload_pdf')->name('upload_pdf_anotator');


// visualisasi
Route::get('/visualisasi', 'VisualisasiController@index')->name('visualisasi');
Route::get('/data_json', 'VisualisasiController@data_json')->name('data_json');

Route::get('/tiny', 'BuilderController@tiny')->name('tiny');
Route::post('/tiny', 'BuilderController@tiny_post')->name('tinymce');

Route::get('/tes-xml', 'HomeController@tes_xml')->name('tes_xml');
Route::post('/jenis_dokumen_ajax', 'BuilderController@jenis_dokumen_ajax')->name('jenis_dokumen_ajax');

Route::get('/komparasi', 'KomparasiController@index')->name('komparasi');
Route::get('/komparasi/select_draft', 'KomparasiController@select_draft')->name('select_draft');
Route::post('/komparasi/cek_konflik', 'KomparasiController@cek_konflik')->name('cek_konflik');



