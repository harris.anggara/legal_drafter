<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/xml-draft', 'ApiController@xml_draft')->name('xml_draft');


Route::get('/xml/{id}', 'ApiController@return_xml')->name('return_xml');
Route::get('/user', function (Request $request) {
    return $request->user();
});

