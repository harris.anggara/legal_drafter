<style type="text/css">
	@font-face {
	    font-family: bookos;
	    src: url('public/fonts/BOOKOS.TTF');
	}
	@page { 
		font-family: bookos;
		line-height: 2;
		font-size: 12;
		margin-left: 3cm;
		margin-top: 5cm;
		margin-right: 2.5cm;
		margin-bottom: 2.5cm;
	 }
</style>
<p style="text-align: center; ">KEPUTUSAN KEPALA BADAN STANDARDISASI NASIONAL</p>
<p style="text-align: center; ">NOMOR {{$nomor_sni}}</p>
<p style="text-align: center; ">TENTANG</p>
<p style="text-align: center; ">PENETAPAN STANDAR NASIONAL INDONESIA {{$judul}} DALAM BAHASA INDONESIA</p>
<p style="text-align: center; ">KEPALA BADAN STANDARDISASI NASIONAL</p>
<p style="text-align: center; "><br></p>
<table class="table table-bordered">
	<tbody>
		@php $first_menimbang = true ; $a = 0; @endphp
		@foreach($menimbang as $v)
			<tr>
				<td style="width: 15%;vertical-align: top;">@if($first_menimbang) Menimbang @endif</td>
				<td style="width: 10px;vertical-align: top;">@if($first_menimbang):@endif</td>
				<!-- <td style="width: 5%">:</td> -->
				<td style="vertical-align: top;width: 5%;text-align: left">{{$alphabet[$a++]}}.</td>
				<td style="text-align: justify;">{{$v}}<br></td>
			</tr>
			@php $first_menimbang = false @endphp
		@endforeach 
		
		@php $first_mengingat = true ; $b=1 @endphp
		@foreach($mengingat as $v)

			<tr>
				<td style="vertical-align: top;">@if($first_mengingat) Mengingat @endif</td>
				<td style="width: 10px;vertical-align: top;">@if($first_mengingat):@endif</td>
				<!-- <td>:</td> -->
				<td style="vertical-align: top;width: 5%;text-align: left">{{$b++}}.</td>
				<td style="text-align: justify;">{{$v}}</td>
			</tr>
			@php $first_mengingat = false @endphp
		@endforeach 

		<tr><td style="vertical-align: top;">Memperhatikan</td>
			<td style="width: 10px;vertical-align: top;">:</td>
			<td colspan="2" style="text-align: justify;">{{$memperhatikan}}<br></td>
		</tr>
		<tr>
			<td><br></td>
			<td><br></td>
			<td colspan="2" style="text-align: center; ">MEMUTUSKAN</td>
		</tr>
		<tr>
			<td style="vertical-align: top;">Menetapkan</td>
			<td style="width: 5%; vertical-align: top;">:</td>
			<td colspan="2" style="text-align: justify;">KEPUTUSAN KEPALA BADAN STANDARDISASI NASIONAL TENTANG PENETAPAN STANDAR NASIONAL INDONESIA {{$judul}}.<br></td>
		</tr>
		@php $i = 1 @endphp
		@foreach($menetapkan as $v)
			<tr>
			<td style="vertical-align: top;">{{strtoupper(makeOrdinal($i++))}}</td>
			<td style="width: 5%;vertical-align: top;">:</td>
			<td colspan="2" style="text-align: justify;">{{$v}}</td>
		</tr>
		@endforeach 
		
	</tbody>
</table>
<?php
setlocale(LC_TIME, 'id_ID.utf8');
?>
<div style="height: 10px"></div>
<table style="width: 100%">
	<tr>
		<td style="width: 43%"></td>
		<td style="width: 57%">
			<p style="">Ditetapkan di Jakarta<br>
			pada tanggal {{strftime("%e %B %G")}}<br>
			KEPALA BADAN STANDARDISASI NASIONAL</p>
			<p style="height: 2cm"></p>
			<p style="text-align: center;">BAMBANG PRASETYA</p>
		</td>
	</tr>
</table>
