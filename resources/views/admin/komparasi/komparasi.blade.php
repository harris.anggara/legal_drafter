@extends('layouts.lte-app')

@push('before-styles')
<!-- <link href="{{asset('plugins/select2/select2.min.css')}}" /> -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
  #source, #target {
    min-width: 400px;
  }
</style>


@endpush
@section('content')
<div class="box box-info">
  <div class="box-header with-border text-center">
    <!-- <i class="fa fa-file"></i> -->

    <h3 class="box-title">Uji Konsistensi Derivatif</h3>
    <!-- tools box -->
    
  <!-- /. tools -->
  </div>
  <div class="box-body">
    <div class="row">
      <div class="col-md-5 text-center">
        <h4>Dokumen Sumber</h4>
        <div class="jumbotron text-center">

          <select id="source">
            <option value="">Pilih Dokumen</option>
          </select>
        </div>
      </div>
      <div class="col-md-2 text-center" style="margin-top: 80px">
        <button class="btn btn-success" id="cek">Uji</button>
      </div>
      <div class="col-md-5 text-center">
        <h4>Dokumen Target</h4>
        <div class="jumbotron text-center">
          <select id="target">
            <option value="">Pilih Dokumen</option>
          </select>
        </div>
      </div>
    </div>
    <div class="row" id="status-info">
      
    </div>
  </div>
  <div class="box-footer clearfix">
  </div>
</div>



@endsection


@push('scripts')
<!-- <script src="{{asset('plugins/select2/select2.min.js')}}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
  

  $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

  var konflik_true = '<div class="callout callout-success">'+
                        '<h4>Sukses</h4>'+
                        '<p>Tidak ditemukan adanya konflik.</p>'+
                      '</div>';
  var konflik_false = '<div class="callout callout-danger">'+
                        '<h4>Konflik!</h4>'+
                        '<p>Ditemukan adanya konflik.</p>'+
                      '</div>';
  var konflik_info = '<div class="callout callout-info">'+
                        '<h4>I am an info callout!</h4>'+
                        '<p>Follow the steps to continue to payment.</p>'+
                      '</div>';
  $(document).ready(function(){
    $('#cek').on('click',function(){
      var val_1 = $('#source').val();
      var val_2 = $('#target').val();

      $.ajax({
        type: "POST",
        url: "{{route('cek_konflik')}}", 
        data: {val_1:val_1, val_2:val_2},
        success: function(result){
          // alert(result);
          switch(result.code) {
              case 0:
                  $('#status-info').html(konflik_false)
                  break;
              case 1:
                  $('#status-info').html(konflik_true)
                  break;
              case 2:
                  $('#status-info').html('<div class="callout callout-warning">'+
                    '<h4>Informasi</h4>'+
                    '<p>Peringatan : '+result.msg+'</p>'+
                  '</div>')
                  break;
              case 3:
                  $('#status-info').html('<div class="callout callout-warning">'+
                                          '<h4>Informasi</h4>'+
                                          '<p>Peringatan : '+result.msg+'</p>'+
                                        '</div>')
                  break;
              default:
          }

          // if(result == 'true'){
            
          // } else if(result == 'false'){
            
          // } else {
            
          // }
          
        }
      });
    });
    $('#source').select2({
      ajax: {
        url: '{{route('select_draft')}}',
        dataType: 'json'
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
      }
    });
    $('#target').select2({
      ajax: {
        url: '{{route('select_draft')}}',
        dataType: 'json'
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
      }
    });
  })
</script>
@endpush