@extends('layouts.lte-app')

@push('before-styles')
@endpush
<style type="text/css">
  bab {
    display: block;  important!
  }

  .mce-visualblocks bab {
    padding-top: 10px; important!
    border: 1px dashed #BBB;
    margin-left: 3px;
    background-image: url(data:image/gif;base64,R0lGODlhCQAJAJEAAAAAAP///7u7u////yH5BAEAAAMALAAAAAAJAAkAAAIQnG+CqCN/mlyvsRUpThG6AgA7);
    background-repeat: no-repeat;
  }
</style>
@section('content')
<style type="text/css">
  
</style>
<bab>adfadfadsfasd
fasdfasdfsadfasdfasdfasdfasdfdf</bab>
<form method="post" action="{{route('tinymce')}}">
  {{csrf_field()}}
  <textarea name="draft"></textarea>
  <button>submit</button>
</form>


@endsection


@push('scripts')
<script type="text/javascript" src="{{asset('plugins/tinymce/js/tinymce/jquery.tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    tinymce.init({
      selector: 'textarea',
      height: 500,
      extended_valid_elements : "bab",
      custom_elements: "bab",
      plugins: 'visualblocks',
        content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
      ],
      style_formats: [
        { title: 'Peraturan', items: [
          // { title: 'Bab', block: 'bab' , style:{
          //   'padding-top' : '10px',
          //   'border': '1px dashed #BBB',
          //   'margin-left': '3px',
          //   'background-image': 'url(data:image/gif;base64,R0lGODlhCQAJAJEAAAAAAP///7u7u////yH5BAEAAAMALAAAAAAJAAkAAAIQnG+CqCN/mlyvsRUpThG6AgA7)',
          //   'background-repeat': 'no-repeat'
          // }},
          { title: 'Bab', block: 'bab' , styles: { "padding-top" : '10px', border: '1px dashed #BBB', 'margin-left': '3px', 'background-image': 'url(data:image/gif;base64,R0lGODlhCQAJAJEAAAAAAP///7u7u////yH5BAEAAAMALAAAAAAJAAkAAAIQnG+CqCN/mlyvsRUpThG6AgA7)',}},
          { title: 'Pasal', block: 'h2' },
          { title: 'Item', block: 'h3' },
          { title: 'Sub Item', block: 'h4' },
          { title: 'Sub Sub Item', block: 'h5' },
          { title: 'Sub Sub Sub Item', block: 'h6' }
        ] },

        { title: 'Headers', items: [
          { title: 'h1', block: 'h1' ,styles: { color: '#ff0000' }},
          { title: 'h2', block: 'h2' },
          { title: 'h3', block: 'h3' },
          { title: 'h4', block: 'h4' },
          { title: 'h5', block: 'h5' },
          { title: 'h6', block: 'h6' }
        ] },

        { title: 'Blocks', items: [
          { title: 'p', block: 'p' },
          { title: 'div', block: 'div' },
          { title: 'pre', block: 'pre' }
        ] },

        { title: 'Containers', items: [
          { title: 'section', block: 'section', wrapper: true, merge_siblings: false },
          { title: 'article', block: 'article', wrapper: true, merge_siblings: false },
          { title: 'blockquote', block: 'blockquote', wrapper: true },
          { title: 'hgroup', block: 'hgroup', wrapper: true },
          { title: 'aside', block: 'aside', wrapper: true },
          { title: 'figure', block: 'figure', wrapper: true }
        ] }
      ],
      visualblocks_default_state: true,
      end_container_on_empty_block: true
     });
  })
</script>
@endpush