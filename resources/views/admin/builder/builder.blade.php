@extends('layouts.lte-app')

@section('content')
<style type="text/css">
        .input-info{
          font-size: 1em;
        }
        /*  bhoechie tab */
        div.bhoechie-tab-container{
          z-index: 10;
          background-color: #ffffff;
          padding: 0 !important;
          
        }
        div.bhoechie-tab-menu{
          padding-right: 0;
          padding-left: 0;
          padding-bottom: 0;
        }
        div.bhoechie-tab-menu div.list-group{
          margin-bottom: 0;
        }
        div.bhoechie-tab-menu div.list-group>a{
          margin-bottom: 0;
        }
        div.bhoechie-tab-menu div.list-group>a .glyphicon,
        div.bhoechie-tab-menu div.list-group>a .fa {
          color: black;
        }
        div.bhoechie-tab-menu div.list-group>a:first-child{
          /*border-top-right-radius: 0;
          -moz-border-top-right-radius: 0;*/
        }
        div.bhoechie-tab-menu div.list-group>a:last-child{
          /*border-bottom-right-radius: 0;
          -moz-border-bottom-right-radius: 0;*/
        }
        div.bhoechie-tab-menu div.list-group>a.active,
        div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
        div.bhoechie-tab-menu div.list-group>a.active .fa{
          background-color: #dadada;
          background-image: #5A55A3;
          color: black;
        }
        div.bhoechie-tab-menu div.list-group>a.active:after{
          content: '';
          position: absolute;
          left: 100%;
          top: 50%;
          margin-top: -13px;
          border-left: 0;
          border-bottom: 13px solid transparent;
          border-top: 13px solid transparent;
          border-left: 10px solid #dd4b39;
        }

        div.bhoechie-tab-content{
          background-color: #ffffff;
          /* border: 1px solid #eeeeee; */
          padding-left: 20px;
          padding-top: 10px;
        }

        div.bhoechie-tab div.bhoechie-tab-content:not(.active){
          display: none;
        }
        div.bhoechie-tab{
          /*border-radius: 4px;*/
          /*-moz-border-radius: 4px;*/
          border:1px solid #ddd;
          /*margin-top: 20px;
          margin-left: 50px;*/
          -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
          box-shadow: 0 6px 12px rgba(0,0,0,.175);
          -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
          background-clip: padding-box;
          opacity: 0.97;
          filter: alpha(opacity=97);
        }
      </style>
      <div class="alert  alert-dismissible bg-gray text-center">
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-preview">Preview</button>
      </div>
      <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 bhoechie-tab-menu">
              <div class="list-group">
                <a href="#" class="list-group-item active text-center">
                  <h4>Bag I</h4>
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4>Bag II</h4>
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4>Bag III</h4>
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4>Bag IV</h4>
                </a>
              </div>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 bhoechie-tab">
                <!-- flight section -->
                <div class="bhoechie-tab-content active">
                  <form save="true" id="bab_1" method="post" action="{{route('builder')}}">
                    {{ csrf_field() }}
                    <input type="hidden" id="id" name="id" value="">
                    <div id="judul_group">
                      <div class="form-group">
                        <label for="judul" class="control-label">A. Judul</label><br>
                          <!-- <small class="input-info"></small>
                          <input type="text" class=" form-control" id="judul" name="judul" placeholder="">-->
                      </div>
                      <div class="form-group col-md-4">
                        <label for="jenis" class="control-label">Jenis:</label><br>
                          <small class="input-info"></small>
                          <input type="text" class="form-control" id="jenis" name="jenis" placeholder="">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="nomor" class="control-label">Nomor:</label><br>
                          <small class="input-info"></small>
                          <input type="text" class="form-control" id="nomor" name="nomor" placeholder="">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="tahun" class="control-label">Tahun:</label><br>
                          <small class="input-info"></small>
                          <input type="text" class="form-control" id="tahun" name="tahun" placeholder="">
                      </div>
                      <div class="form-group col-md-12">
                        <label for="judul" class="control-label">Judul:</label><br>
                          <small class="input-info"></small>
                          <input type="text" class="form-control" id="judul" name="judul" placeholder="">
                      </div>
                      
                    </div>
                    <div id="pembukaan_group">
                      <div class="form-group">
                        <label for="pembukaan" class="control-label">B. Pembukaan:</label><br>
                          <!-- <small class="input-info"></small>
                          <input type="text" class="form-control" id="pembukaan" name="pembukaan" placeholder=""> -->
                      </div>
                      <div class="form-group col-md-12">
                        <label for="frasa" class="control-label">Frasa:</label><br>
                          <small class="input-info"></small>
                          <input type="text" class="form-control" id="frasa" name="frasa" value="Dengan Rahmat Tuhan Yang Maha Esa" placeholder="" readonly>
                      </div>
                      <div class="form-group col-md-12">
                        <label for="jabatan_pembentuk" class="control-label">Jabatan Pembentuk:</label><br>
                          <small class="input-info"></small>
                          <input type="text" class="form-control" id="jabatan_pembentuk" name="jabatan_pembentuk" value="" placeholder="">
                      </div>
                      <div class="form-group col-md-12">
                        <label for="konsiderans" class="control-label">Konsiderans:</label><br>
                        <div id="konsiderans_group">
                          <div class="row form-group">
                            <div class="row form-group">
                              <div class="konsiderans_item">
                                <small class="input-info col-md-1">(1) </small>
                                <div class="col-md-10">
                                  <textarea type="text" class="form-control col-md-10" id="konsiderans" name="konsiderans[0]["val"]" value="" placeholder=""> </textarea>
                                </div>
                                <div class="col-md-1">
                                  <button value="konsiderans" id="konsiderans_rinci" class="btn btn-success btn-xs btn_rinci"><i class="fa fa-plus"></i></button>
                                  <button value="konsiderans" id="konsiderans_rinci_kur" class="btn btn-warning btn-xs btn_rinci_kur"><i class="fa fa-minus"></i></button>
                                </div>
                              </div>
                            </div>
                            <div class=" col-md-offset-1 col-md-11">
                              <div id="konsiderans_sub">
                                
                              </div>
                              
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="col-md-11 col-md-offset-1">
                        <div id="konsiderans_btn" class="form-group">
                          <button id="konsiderans_tam" class="btn btn-info btn-xs btn-tam" value="konsiderans">Tambah</button>
                          <button id="konsiderans_kur" class="btn btn-warning btn-xs btn-kur" value="konsiderans">Kurang</button>
                        </div>
                      </div>
                      
                      <div class="form-group col-md-12">
                        <label for="dasar_hukum" class="control-label">Dasar Hukum:</label><br>
                        <div id="dasar_hukum_group">
                          <div class="row form-group">
                            <div class="row form-group">
                              <div class="dasar_hukum_item">
                                <small class="input-info col-md-1">(1) </small>
                                <div class="col-md-10">
                                  <textarea type="text" class="form-control col-md-10" id="dasar_hukum" name="dasar_hukum[0]["val"]" value="" placeholder=""> </textarea>
                                </div>
                                <div class="col-md-1">
                                  <button value="dasar_hukum" id="dasar_hukum_rinci" class="btn btn-success btn-xs btn_rinci"><i class="fa fa-plus"></i></button>
                                  <button value="dasar_hukum" id="dasar_hukum_rinci_kur" class="btn btn-warning btn-xs btn_rinci_kur"><i class="fa fa-minus"></i></button>
                                </div>
                              </div>
                            </div>
                            <div class=" col-md-offset-1 col-md-11">
                              <div id="dasar_hukum_sub">

                              </div>
                            </div>
                          </div>
                          
                        </div>
                      </div>
                      
                      <div class="col-md-11 col-md-offset-1">
                        <div id="dasar_hukum_btn" class="form-group">
                          <button id="dasar_hukum_tam" class="btn btn-info btn-xs btn-tam" value="dasar_hukum">Tambah</button>
                          <button id="dasar_hukum_kur" class="btn btn-warning btn-xs btn-kur" value="dasar_hukum">Kurang</button>
                        </div>
                      </div>
                      
                      <div class="form-group col-md-12">
                        <label for="memutuskan" class="control-label">Memutuskan:</label><br>
                          <small class="input-info"></small>
                          <input type="text" class="form-control" id="memutuskan" name="memutuskan" placeholder="">
                      </div>
                    </div>
                    <div id="batang_tubuh_group">
                      <div>
                        <!-- <label for="pembukaan" class="control-label">C. Batang Tubuh:</label><br> -->
                        <div class="text-center"><h3>Bab I</h3></div>
                        <div class="text-center"><h3>Ketentuan Umum</h3></div>
                          <!-- <small class="input-info"></small>
                          <input type="text" class="form-control" id="pembukaan" name="pembukaan" placeholder=""> -->
                      </div>
                      <div class="form-group col-md-12">
                        <!-- <label for="ketentuan_umum" class="control-label">Ketentuan Umum:</label><br> -->
                        <div id="ketentuan_umum_group">
                          <div class="row form-group">
                            <div class="row form-group">
                              <div class="ketentuan_umum_item">
                                <small class="input-info col-md-1">pasal 1 </small>
                                <div class="col-md-10">
                                  <textarea type="text" class="form-control col-md-10" id="ketentuan_umum" name="ketentuan_umum[0]["val"]" value="" placeholder=""> </textarea>
                                </div>
                                <div class="col-md-1">
                                  <button value="ketentuan_umum" id="ketentuan_umum_rinci" class="btn btn-success btn-xs btn_rinci"><i class="fa fa-plus"></i></button>
                                  <button value="ketentuan_umum" id="ketentuan_umum_rinci_kur" class="btn btn-warning btn-xs btn_rinci_kur"><i class="fa fa-minus"></i></button>
                                </div>
                              </div>
                            </div>
                            <div class=" col-md-offset-1 col-md-11">
                              <div id="ketentuan_umum_sub">
                                
                              </div>
                              
                            </div>
                          </div>
                        </div>
                      </div>
                      

                      <div class="col-md-11 col-md-offset-1">
                        <div id="ketentuan_umum_btn" class="form-group">
                          
                          <button id="ketentuan_umum_tam" class="btn btn-info btn-xs btn-tam" value="ketentuan_umum">Tambah</button>
                          <button id="ketentuan_umum_kur" class="btn btn-warning btn-xs btn-kur" value="ketentuan_umum">Kurang</button>
                        </div>
                      </div>
                      
                      <div class="form-group col-md-12">
                        <label for="materi_pokok" class="control-label">Materi Pokok Yang Diatur:</label><br>
                        <div id="materi_pokok_group">
                          <div class="row form-group">
                            <div class="row form-group">
                              <div class="materi_pokok_item">
                                <small class="input-info col-md-1">Pasal 1 </small>
                                <div class="col-md-10">
                                  <textarea type="text" class="form-control col-md-10" id="materi_pokok" name="materi_pokok[0]["val"]" value="" placeholder=""> </textarea>
                                </div>
                                <div class="col-md-1">
                                  <button id="materi_pokok_rinci" class="btn btn-success btn-xs btn_rinci" value="materi_pokok"><i class="fa fa-plus"></i></button>
                                  <button value="materi_pokok" id="materi_pokok_rinci_kur" class="btn btn-warning btn-xs btn_rinci_kur"><i class="fa fa-minus"></i></button>
                                </div>
                              </div>
                            </div>
                            <div class=" col-md-offset-1 col-md-11">
                              <div id="materi_pokok_sub">
                                
                              </div>
                              
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="col-md-11 col-md-offset-1">
                        <div id="materi_pokok_btn" class="form-group">
                          <button id="materi_pokok_tam" class="btn btn-info btn-xs btn-tam" value="materi_pokok">Tambah</button>
                          <button id="materi_pokok_kur" class="btn btn-warning btn-xs btn-kur" value="materi_pokok">Kurang</button>
                        </div>
                        
                      </div>

                      <div class="form-group col-md-12">
                        <label for="ketentuan_pidana" class="control-label">Ketentuan Pidana:</label><br>
                        <div id="ketentuan_pidana_group">
                          <div class="row form-group">
                            <div class="row form-group">
                              <div class="ketentuan_pidana_item">
                                <small class="input-info col-md-1">Pasal 1 </small>
                                <div class="col-md-10">
                                  <textarea type="text" class="form-control col-md-10" id="ketentuan_pidana" name="ketentuan_pidana[0]["val"]" value="" placeholder=""> </textarea>
                                </div>
                                <div class="col-md-1">
                                  <button id="ketentuan_pidana_rinci" class="btn btn-success btn-xs btn_rinci" value="ketentuan_pidana"><i class="fa fa-plus"></i></button>
                                  <button value="ketentuan_pidana" id="ketentuan_pidana_rinci_kur" class="btn btn-warning btn-xs btn_rinci_kur"><i class="fa fa-minus"></i></button>
                                </div>
                              </div>
                            </div>
                            <div class=" col-md-offset-1 col-md-11">
                              <div id="ketentuan_pidana_sub">
                                
                              </div>
                              
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="col-md-11 col-md-offset-1">
                        <div id="ketentuan_pidana_btn" class="form-group">
                          <button id="ketentuan_pidana_tam" class="btn btn-info btn-xs btn-tam" value="ketentuan_pidana">Tambah</button>
                          <button id="ketentuan_pidana_kur" class="btn btn-warning btn-xs btn-kur" value="ketentuan_pidana">Kurang</button>
                        </div>
                        
                      </div>
                      
                      <div class="form-group col-md-12">
                        <label for="ketentuan_peralihan" class="control-label">Ketentuan Peralihan:</label><br>
                        <div id="ketentuan_peralihan_group">
                          <div class="row form-group">
                            <div class="row form-group">
                              <div class="ketentuan_peralihan_item">
                                <small class="input-info col-md-1">Pasal 1 </small>
                                <div class="col-md-10">
                                  <textarea type="text" class="form-control col-md-10" id="ketentuan_peralihan" name="ketentuan_peralihan[0]["val"]" value="" placeholder=""> </textarea>
                                </div>
                                <div class="col-md-1">
                                  <button id="ketentuan_peralihan_rinci" class="btn btn-success btn-xs btn_rinci" value="ketentuan_peralihan"><i class="fa fa-plus"></i></button>
                                  <button value="ketentuan_peralihan" id="ketentuan_peralihan_rinci_kur" class="btn btn-warning btn-xs btn_rinci_kur"><i class="fa fa-minus"></i></button>
                                </div>
                              </div>
                            </div>
                            <div class=" col-md-offset-1 col-md-11">
                              <div id="ketentuan_peralihan_sub">
                                
                              </div>
                              
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="col-md-11 col-md-offset-1">
                        <div id="ketentuan_peralihan_btn" class="form-group">
                          <button id="ketentuan_peralihan_tam" class="btn btn-info btn-xs btn-tam" value="ketentuan_peralihan">Tambah</button>
                          <button id="ketentuan_peralihan_kur" class="btn btn-warning btn-xs btn-kur" value="ketentuan_peralihan">Kurang</button>
                        </div>
                        
                      </div>

                      <div class="form-group col-md-12">
                        <label for="ketentuan_penutup" class="control-label">Ketentuan Penutup:</label><br>
                        <div id="ketentuan_penutup_group">
                          <div class="row form-group">
                            <div class="row form-group">
                              <div class="ketentuan_penutup_item">
                                <small class="input-info col-md-1">Pasal 1 </small>
                                <div class="col-md-10">
                                  <textarea type="text" class="form-control col-md-10" id="ketentuan_penutup" name="ketentuan_penutup[0]["val"]" value="" placeholder=""> </textarea>
                                </div>
                                <div class="col-md-1">
                                  <button value="ketentuan_penutup" id="ketentuan_penutup_rinci" class="btn btn-success btn-xs btn_rinci"><i class="fa fa-plus"></i></button>

                                  <button value="ketentuan_penutup" id="ketentuan_penutup_rinci_kur" class="btn btn-warning btn-xs btn_rinci_kur"><i class="fa fa-minus"></i></button>
                                </div>
                              </div>
                            </div>
                            <div class=" col-md-offset-1 col-md-11">
                              <div id="ketentuan_penutup_sub">
                                
                              </div>
                              
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="col-md-11 col-md-offset-1">
                        <div id="ketentuan_penutup_btn" class="form-group">
                          <button id="ketentuan_penutup_tam" class="btn btn-info btn-xs btn-tam" value="ketentuan_penutup">Tambah</button>
                          <button id="ketentuan_penutup_kur" class="btn btn-warning btn-xs btn-kur" value="ketentuan_penutup">Kurang</button>
                        </div>
                        
                      </div>

                    </div>
                    <div id="batang_tubuh_group">
                      <div class="form-group">
                        <label for="penutup" class="control-label">D. Penutup:</label><br>
                          <!-- <small class="input-info"></small>
                          <input type="text" class="form-control" id="pembukaan" name="pembukaan" placeholder=""> -->
                      </div>
                      <div class="form-group col-md-12">
                        <div id="penutup_group">
                          <div class="row form-group">
                            <div class="row form-group">
                              <div class="penutup_item">
                                <small class="input-info col-md-1">Pasal 1 </small>
                                <div class="col-md-10">
                                  <textarea type="text" class="form-control col-md-10" id="penutup" name="penutup[0]["val"]" value="" placeholder=""> </textarea>
                                </div>
                                <div class="col-md-1">
                                  <button value="penutup" id="penutup_rinci" class="btn btn-xs btn-success btn_rinci"><i class="fa fa-plus"></i></button>

                                  <button value="penutup" id="penutup_rinci_kur" class="btn btn-warning btn-xs btn_rinci_kur"><i class="fa fa-minus"></i></button>
                                </div>
                              </div>
                            </div>
                            <div class=" col-md-offset-1 col-md-11">
                              <div id="penutup_sub">
                                
                              </div>
                              
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="col-md-11 col-md-offset-1">
                        <div id="penutup_btn" class="form-group ">
                          <button id="penutup_tam" class="btn btn-info btn-xs btn-tam" value="penutup">Tambah</button>
                          <button id="penutup_kur" class="btn btn-warning btn-xs btn-kur" value="penutup">Kurang</button>
                        </div>
                      </div>
                    </div>
                    <div class="text-center">
                      <button type="submit" class="btn btn-primary" id="submit_bab_1"> Simpan</button>
                    </div>
                  </form>
                    
                </div>
                <!-- train section -->
                <div class="bhoechie-tab-content">
                    <center>
                      <h1 class="" style="font-size:12em;color:#55518a"> <i class="fa fa-clock"></i></h1>
                      <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                    </center>
                    <button class="prev">prev</button>
                    <button class="next">next</button>
                </div>
    
                <!-- hotel search -->
                <div class="bhoechie-tab-content">
                    <center>
                      <h1 class="" style="font-size:12em;color:#55518a"> <i class="fa fa-clock"></i></h1>
                      <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                    </center>
                </div>
                <div class="bhoechie-tab-content">
                    <center>
                      <h1 class="" style="font-size:12em;color:#55518a"> <i class="fa fa-clock"></i></h1>
                      <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                    </center>
                </div>
            </div>
      </div>
    </div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-preview">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <style type="text/css">
            iframe,.no_iframe {
              width: 100%;
              height: 400px;
              border: 1px solid #666;
              background-color: #ddd;
            }.no_iframe > div {
              width: 80%;
              position: absolute;
              top: 50%;
              left: 50%;
              -webkit-transform: translate(-50%, -50%);
                      transform: translate(-50%, -50%);
              font-weight: normal;
            }
            .modal-body>.overlay{
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background: rgba(255,255,255,0.7);
            }

            .modal-body .overlay>.fa{
                position: absolute;
                top: 50%;
                left: 50%;
            }
          </style>
          <iframe id="pdf_preview" name="pdf_preview" type="application/pdf"></iframe>
          <div class="overlay" id="overlay-pdf">
            <i class="fa fa-sync fa-spin"></i>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@push('scripts')
<script type="text/javascript">
  var counter = [];
  var ctr = [];
  $(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
    $(".next").click(function(e) {
        e.preventDefault();
        var par = $(this).parent();
        var index = par.index();
        $("div.bhoechie-tab-menu>div.list-group>a").removeClass("active");
        $("div.bhoechie-tab-menu>div.list-group>a").eq(index+1).addClass("active");

        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index+1).addClass("active");
    });
    $(".prev").click(function(e) {
        e.preventDefault();
        var par = $(this).parent();
        var index = par.index();
        $("div.bhoechie-tab-menu>div.list-group>a").removeClass("active");
        $("div.bhoechie-tab-menu>div.list-group>a").eq(index-1).addClass("active");

        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index-1).addClass("active");
    });
    $(document).on('click','.btn-tam',function(e) {
        e.preventDefault();
        var val = $(this).val();
        if(ctr[val] == undefined){
          ctr[val] = 0;
        } else {
          ctr[val]++;
        }

        var html =  '<div class="row">'+
                      '<div class="row form-group">'+
                        '<div class="konsiderans_item">'+
                          '<small class="input-info col-md-1"> Pasal '+(ctr[val]+2)+' </small>'+
                          '<div class="col-md-10">'+
                            '<textarea type="text" class="form-control col-md-10" id="'+val+'-'+ctr[val]+'" name="'+val+'['+(ctr[val]+1)+'][val]" value="" placeholder=""> </textarea>'+
                          '</div>'+
                          '<div class="col-md-1">'+
                            '<button value="'+ctr[val]+'-'+val+'" id="konsiderans_rinci" class="btn btn-success btn-xs btn_rinci"><i class="fa fa-plus"></i></button>'+
                            '<button value="'+ctr[val]+'-'+val+'" id="konsiderans_rinci_kur" class="btn btn-warning btn-xs btn_rinci_kur"><i class="fa fa-minus"></i></button>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                      '<div class=" col-md-offset-1 col-md-11">'+
                        '<div id="'+ctr[val]+'-'+val+'_sub">'+
                          
                        '</div>'+
                        
                      '</div>'+
                    '</div>';

        $('#'+val+'_group').append(html);
      });
    $(document).on('click','.btn_rinci_kur',function(e) {
       e.preventDefault();
        var val = $(this).val();
        // alert(val);
        $('.'+val+'_sub_group:last-child').remove();
    });
    $(document).on('click','.btn_rinci',function(e) {
       e.preventDefault();


        // var val = $(this).val();
        var val_full = $(this).val();
        var result = $(this).val().split('-');
        var ct = result[0];
        var val = result[1];
        // alert(val_full);
        if(counter[val] == undefined){
          counter[val] = 0;
        } else {
          counter[val]++;
        }
        var sub_html =  '<div id="'+counter[val]+'-'+val+'_sub_group" class="'+val+'_sub_group">'+
                          '<div class="row form-group">'+
                            '<div class="'+counter[val]+'-'+val+'_sub_item">'+
                              '<small class="input-info col-md-1">('+(counter[val]+1)+') </small>'+
                              '<div class="col-md-10">'+
                                '<textarea type="text" class="form-control col-md-10" id="'+counter[val]+'-'+val+'" name="'+val+'['+ct+']'+'[sub]['+counter[val]+'][val]" value="" placeholder=""> </textarea>'+
                              '</div>'+
                              '<div class="col-md-1">'+
                                '<button value="'+counter[val]+'-'+ct+'-'+val+'" id="'+counter[val]+'-'+val+'_rinci" class="btn btn-success btn-xs btn_rinci_sub"><i class="fa fa-plus"></i> </button>'+
                                '<button value="'+counter[val]+'-'+ct+'-'+val+'" id="'+counter[val]+'-'+val+'_rinci" class="btn btn-warning btn-xs btn_rinci_sub_kur"><i class="fa fa-minus"></i> </button>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                          '<div class=" col-md-offset-1 col-md-11">'+
                            '<div id="'+counter[val]+'-'+ct+'-'+val+'_sub_sub">'+
                            '</div>'+
                          '</div>'+
                        '</div>';
        $('#'+val_full+'_sub').append(sub_html);
    });

    $(document).on('click','.btn_rinci_sub_kur',function(e) {
       e.preventDefault();
        var val = $(this).val();
        // alert(val);
        $('.'+val+'_sub_sub_group:last-child').remove();
    });

    $(document).on('click','.btn_rinci_sub',function(e) {
        e.preventDefault();
        var val_full = $(this).val();
        var result = $(this).val().split('-');
        var ct = result[0]
        var ct2 = result[1];
        var val = result[2];
        // alert(val);
        if(counter[val_full] == undefined){
          counter[val_full] = 0;
        } else {
          counter[val_full]++;
        }
        var sub_sub_html ='<div id="'+counter[val_full]+'-'+val_full+'_sub_sub_group" class="'+val_full+'_sub_sub_group">'+
                            '<div class="row form-group">'+
                              '<div class="'+counter[val_full]+'-'+val_full+'_sub_sub_item">'+
                                '<small class="input-info col-md-1">a. </small>'+
                                '<div class="col-md-10">'+
                                  '<textarea type="text" class="form-control col-md-10" id="'+counter[val_full]+'-'+val_full+'" name="'+val+'['+ct2+'][sub]['+ct+'][sub_sub]['+counter[val_full]+'][val]" value="" placeholder=""> </textarea>'+
                                '</div>'+
                                '<div class="col-md-1">'+
                                  '<button value="'+counter[val_full]+'-'+val_full+'" id="'+counter[val_full]+'-'+val_full+'_rinci" class="btn btn-success btn-xs btn_rinci_sub_sub"><i class="fa fa-plus"></i></button>'+
                                  '<button value="'+counter[val_full]+'-'+val_full+'" id="'+counter[val_full]+'-'+val_full+'_rinci" class="btn btn-warning btn-xs btn_rinci_sub_sub_kur"><i class="fa fa-minus"></i> </button>'+
                                '</div>'+
                              '</div>'+
                            '</div>'+
                            '<div class=" col-md-offset-1 col-md-11">'+
                              '<div id="'+counter[val_full]+'-'+val_full+'_sub_sub_sub">'+
                              '</div>'+
                            '</div>'+
                          '</div>';
        $('#'+val_full+'_sub_sub').append(sub_sub_html);
    });

    $(document).on('click','.btn_rinci_sub_sub_kur',function(e) {
       e.preventDefault();
        var val = $(this).val();
        $('.'+val+'_sub_sub_sub_group:last-child').remove();
    });

    
    $(document).on('click','.btn_rinci_sub_sub',function(e) {
        e.preventDefault();

        var val_full = $(this).val();
        var result = $(this).val().split('-');
        var ct = result[0];
        var ct2 = result[1];
        var ct3 = result[2];
        var val = result[3];


        // alert(val);
        if(counter[val_full] == undefined){
          counter[val_full] = 0;
        } else {
          counter[val_full]++;
        }

        var sub_sub_sub_html ='<div id="'+counter[val_full]+'-'+val_full+'_sub_sub_sub_group" class="'+val_full+'_sub_sub_sub_group">'+
                                '<div class="row form-group">'+
                                  '<div class="'+counter[val_full]+'-'+val_full+'_sub_sub_sub_item">'+
                                    '<small class="input-info col-md-1">1 </small>'+
                                    '<div class="col-md-10">'+
                                      '<textarea type="text" class="form-control col-md-10" id="'+counter[val_full]+'-'+val_full+'" name="'+val+'['+ct3+'][sub]['+ct2+'][sub_sub]['+ct+'][sub_sub_sub]['+counter[val_full]+'][val]" value="" placeholder=""> </textarea>'+
                                    '</div>'+
                                  '</div>'+
                                '</div>'+
                              '</div>';
        $('#'+val_full+'_sub_sub_sub').append(sub_sub_sub_html);
    });



    $('#modal-preview').on('shown.bs.modal', function (e) {
        $('#overlay-pdf').show();
        $('#bab_1').attr("action",'{{route('preview')}}').attr("target",'pdf_preview').attr("save",false).submit();

    });
    $('#pdf_preview').on('load', function() {
      $('#overlay-pdf').hide();
    });

    $('#modal-preview').on('hide.bs.modal', function (e) {
        $('#overlay-pdf').show();
        $('#bab_1').attr("action",'{{route('builder')}}').removeAttr("target").attr("save",true);

    });

    $(document).on('click','#submit_bab_1', function(e){
      e.preventDefault();
      alert('tes');
      var datastring = $("#bab_1").serialize();
      $.ajax({
        type: "POST",
        url: "{{route('builder')}}", 
        data: datastring,
        success: function(result){
          alert(result.msg);
          $('#id').val(result.id);
        }
      });
    })

  });

  function nextChar(c) {
    return String.fromCharCode(c.charCodeAt(0) + 1);
  }
</script>
@endpush