@extends('layouts.lte-app')

@push('before-styles')

  <link rel="stylesheet" href="{{asset('plugins/xonomy/xonomy.css')}}">

@endpush
@section('content')
<div class="box box-info">
  <div class="box-header with-border">
    <i class="fa fa-file"></i>

    <h3 class="box-title">Legal Drafter</h3>
    <!-- tools box -->
    
  <!-- /. tools -->
  </div>
  <div class="box-body">
    <form method="post" action="{{route('builder_post')}}">
      {{csrf_field()}}
      <input type="hidden" id="id" name="id" value="{{$id}}">
      <div class="row">
        <div class="col-md-12">
          <div id="editor"></div>
        </div>
        
      </div>
      
      
  </div>
  <div class="box-footer clearfix">
    <button type="button" class="pull-right btn btn-default" id="btn-xml">Simpan
    <i class="fa fa-arrow-circle-right"></i></button>
  </div>
  </form>
</div>



@endsection


@push('scripts')
<script type="text/javascript" src="{{asset('plugins/xonomy/xonomy.js')}}"></script>
<script type="text/javascript">
  var idJudul = '{{$idJudul}}';
  var jenis = '{{$jenis}}';
  var nomor = '{{$nomor}}';
  var tahun = '{{$tahun}}';
  var konsiderans = 1;

  var counter_bab = 1;
  
  

  var ids2 ={
    idJabatan     :'idJabatan',
    idKonsiderans :'idKonsiderans',
    idDasarHukum  :'idDasarHukum',
    idMemutuskan  :'idMemutuskan',
    idBab         :{
      // idPasal:{
      //   idItemPasal:'idItemPasal',
      // },
      idBagian : {
        idPasal:{
          idItemPasal:'idItemPasal',
        }
      },
      
    }
  };

  $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
  $(document).ready(function(){
    var counter_link = {{$total_link}};
   
    
    function jenis_dokumen(str, callback) {
      // jenis_dokumen_ajax
      // var rs = '';
      $.ajax({
        type: "POST",
        url: "{{route('jenis_dokumen_ajax')}}", 
        data: {str:str},
        success: function(result){
          // alert(result.result);
          
          callback(result.result);
        },
        error: function(result){
          callback(result.result);
        }
      });
      // alert(rs);
    }

    var xml = '{!! $xml !!}';
    var editor=document.getElementById("editor");

    var docSpec={
        onchange: function(data){
          // alert(counter_link++);
          // alert(JSON.stringify(data));
            if(data != undefined){
              var div=document.getElementById(data.htmlID);
              var parent=$("#"+data.htmlID).parent().closest(".element");
              var name = $(parent).attr('data-name');
              console.log('======== '+JSON.stringify(data));

              switch(data.name) {
                  case 'jenis':
                      jenis_dokumen(data.value,function(rs){
                        jenis = rs;
                        update_idJudul();
                      });
                      break;
                  case 'nomor':
                      nomor = data.value;
                      break;
                  case 'tahun':
                      tahun = data.value;
                      break;
                  default:
                      
              }
              update_idJudul();
            }
            

            
            // alert(div);
            // jsElement.name
            // console.log("I been changed now! ");
            // console.log("I been changed now! "+idJudul);
        },
        validate: function(obj){
            // console.log("I be validatin' now!");
            update_idJudul();
        },
        elements: {
            "judul":{
                hasText: true,
                attributes: {
                  "idJudul": {
                    asker: Xonomy.askString
                  },
                  "kodifikasi": {
                    asker: Xonomy.askString
                  },
                  "kompilasi": {
                    asker: Xonomy.askString
                  },
                  "status": {
                    asker: Xonomy.askString
                  },
                  "jenis": {
                    asker: Xonomy.askString
                  },
                  "nomor": {
                    asker: Xonomy.askString
                  },
                  "tahun": {
                    asker: Xonomy.askString
                  },
                },
                menu : [
                {
                    caption: "Add @jenis=\" \"",
                    action: new_attr_xonomy,
                    actionParameter: {name: "jenis", value: ""},
                    hideIf: function(jsElement){
                        return jsElement.hasAttribute("jenis");
                    }
                },
                {
                    caption: "Add @nomor=\"\"",
                    action: new_attr_xonomy,
                    actionParameter: {name: "nomor", value: ""},
                    hideIf: function(jsElement){
                        return jsElement.hasAttribute("nomor");
                    }
                },
                {
                    caption: "Add @tahun=\"\"",
                    action: new_attr_xonomy,
                    actionParameter: {name: "tahun", value: ""},
                    hideIf: function(jsElement){
                        return jsElement.hasAttribute("tahun");
                    }
                },
              ]
            },
            "jenis": {
                hasText: true,
                menu:[
                  { 
                    caption: "Delete <jenis>",
                    action: Xonomy.deleteElement
                  },

                ]
            },
            "nomor": {
                hasText: true,
                menu:[
                  { 
                    caption: "Delete <nomor>",
                    action: Xonomy.deleteElement
                  },

                ]
            },
            "tahun": {
                hasText: true,
                menu:[
                  { 
                    caption: "Delete <tahun>",
                    action: Xonomy.deleteElement
                  },

                ]
            },
            "namaJudul": {
                hasText: true,
                menu:[
                  { 
                    caption: "Delete <namaJudul>",
                    action: Xonomy.deleteElement
                  },

                ]
            },
            "frasa": {
                hasText: true,
            },
            "jabatanPembentuk": {
                hasText: true,
                attributes: {
                  "idJabatan": {
                    asker: Xonomy.askString
                  },
                }
            },
            "konsiderans": {
                hasText: true,
                attributes: {
                  "idKonsiderans": {
                    asker: Xonomy.askString
                  },
                  "link": {
                    asker: Xonomy.askString
                  },
                  @foreach($arr_link as $k => $v)
                    "{{$v}}": {
                      asker: Xonomy.askString,
                      menu: [{
                        caption: "Delete this {{$v}}",
                        action: Xonomy.deleteAttribute
                      }]
                    },
                  @endforeach
                },
                menu : [
                  {
                      caption: "New <konsiderans> before this",
                      action: Xonomy.newElementBefore,
                      actionParameter: "<konsiderans idKonsiderans='k-id-01'></konsiderans>"
                  }, {
                      caption: "New <konsiderans> after this",
                      action: Xonomy.newElementAfter,
                      actionParameter: "<konsiderans idKonsiderans='k-id-01'></konsiderans>"
                  },
                  {
                      caption: "Delete this <konsiderans>",
                      action: Xonomy.deleteElement
                  },
                  {
                        caption: "Add @link=\"\"",
                        action: new_attr_xonomy,
                        actionParameter: {name: "link_"+(counter_link), value: ""},
                        hideIf: function(jsElement){
                            return jsElement.hasAttribute("label");
                        }
                    },
                ]
            },
            "dasarHukum": {
                hasText: true,
                attributes: {
                  "idDasarHukum": {
                    asker: Xonomy.askString
                  },
                },
                menu : [
                  {
                      caption: "New <dasarHukum> before this",
                      action: Xonomy.newElementBefore,
                      actionParameter: "<dasarHukum idDasarHukum='dh-id-1'></dasarHukum>"
                  }, {
                      caption: "New <dasarHukum> after this",
                      action: Xonomy.newElementAfter,
                      actionParameter: "<dasarHukum idDasarHukum='dh-id-1'></dasarHukum>"
                  },
                  {
                      caption: "Delete this <dasarHukum>",
                      action: Xonomy.deleteElement
                  },
                ]
            },
            "memutuskan": {
                hasText: true,
                attributes: {
                  "idMemutuskan": {
                    asker: Xonomy.askString
                  },

                }
            },
            "bab": {
                menu: [
                    {
                        caption: "Append an <pasal>",
                        action: Xonomy.newElementChild,
                        actionParameter: "<pasal idPasal='p-id-1-0-1'></pasal>"
                    },
                    {
                        caption: "Append an <bagian>",
                        action: Xonomy.newElementChild,
                        actionParameter: "<bagian idBagian='bag-id-1-1' nomor='' nama=''></bagian>"
                    },
                    {
                        caption: "Add @label=\"\"",
                        action: new_attr_xonomy,
                        actionParameter: {name: "label", value: ""},
                        hideIf: function(jsElement){
                            return jsElement.hasAttribute("label");
                        }
                    }, {
                        caption: "Delete this <bab>",
                        action: Xonomy.deleteElement
                    }, 
                    {
                        caption: "New <bab> before this",
                        action: Xonomy.newElementBefore,
                        actionParameter: "<bab idBab='b-id-1' nama=''></bab>"
                    }, 
                    {
                        caption: "New <bab> after this",
                        action: Xonomy.newElementAfter,
                        actionParameter: "<bab idBab='b-id-1' nama=''></bab>"
                        // actionParameter: "<bab idBab='b-"+idJudul+"-"+(counter_bab=counter_bab+1)+"' nama=''></bab>"
                    }],
                canDropTo: ["bab"],
                attributes: {
                    "idBab": {
                      asker: Xonomy.askString
                    },
                    "nama": {
                      asker: Xonomy.askString
                    },
                  //   "label": {
                  //   asker: Xonomy.askString
                  // },
                }
            },
            "bagian": {
                menu: [
                    {
                        caption: "Append an <pasal>",
                        action: Xonomy.newElementChild,
                        actionParameter: "<pasal idPasal='p-id-1-1-1'></pasal>"
                    },
                    {
                        caption: "Add @label=\"\"",
                        action: Xonomy.newAttribute,
                        actionParameter: {name: "label", value: ""},
                        hideIf: function(jsElement){
                            return jsElement.hasAttribute("label");
                        }
                    }, {
                        caption: "Delete this <bagian>",
                        action: Xonomy.deleteElement
                    }, {
                        caption: "New <bagian> before this",
                        action: Xonomy.newElementBefore,
                        actionParameter: "<bagian idBagian='bag-id-1-1' nomor='' nama=''></bagian>"
                    }, {
                        caption: "New <bagian> after this",
                        action: Xonomy.newElementAfter,
                        actionParameter: "<bagian idBagian='bag-id-1-1' nomor='' nama=''></bagian>"
                    }],
                canDropTo: ["bab"],
                attributes: {
                  "idBagian": {
                    asker: Xonomy.askString
                  },
                  "nomor": {
                    asker: Xonomy.askString
                  },
                  "nama": {
                    asker: Xonomy.askString
                  },
                }
            },
            "pasal": {
                attributes: {
                  "idPasal": {
                    asker: Xonomy.askString
                  },

                },
                menu: [
                    {
                        caption: "Append an <itemPasal>",
                        action: Xonomy.newElementChild,
                        actionParameter: "<itemPasal idItemPasal='ip-id-1-1-1-1' bentuk='normal'></itemPasal>"
                    },
                    {
                        caption: "Add @label=\"\"",
                        action: Xonomy.newAttribute,
                        actionParameter: {name: "label", value: ""},
                        hideIf: function(jsElement){
                            return jsElement.hasAttribute("label");
                        }
                    }, {
                        caption: "Delete this <pasal>",
                        action: Xonomy.deleteElement
                    }, {
                        caption: "New <pasal> before this",
                        action: Xonomy.newElementBefore,
                        actionParameter: "<pasal idPasal='p-id-1-1-1'></pasal>"
                    }, {
                        caption: "New <pasal> after this",
                        action: Xonomy.newElementAfter,
                        actionParameter: "<pasal idPasal='p-id-1-1-1'></pasal>"
                    }],
                canDropTo: ["pasal"],
                
            },
            "itemPasal": {
                attributes: {
                  "idItemPasal": {
                    asker: Xonomy.askString
                  },
                  "bentuk": {
                    asker: Xonomy.askPicklist,
                    askerParameter: [
                      {value: "normal", caption: "normal"},
                      {value: "item", caption: "item"}
                    ]
                  },

                },
                hasText: true,
                menu: [
                    {
                        caption: "Add @label=\"\"",
                        action: Xonomy.newAttribute,
                        actionParameter: {name: "label", value: ""},
                        hideIf: function(jsElement){
                            return jsElement.hasAttribute("label");
                        }
                    }, {
                        caption: "Delete this <itemPasal>",
                        action: Xonomy.deleteElement
                    }, {
                        caption: "New <itemPasal> before this",
                        action: Xonomy.newElementBefore,
                        actionParameter: "<itemPasal idItemPasal='ip-id-1-1-1-1' bentuk='normal'></itemPasal>"
                    }, {
                        caption: "New <itemPasal> after this",
                        action: Xonomy.newElementAfter,
                        actionParameter: "<itemPasal idItemPasal='ip-id-1-1-1-1' bentuk='normal'></itemPasal>"
                    }],
                canDropTo: ["pasal"],
                
            }
        }
    };

    Xonomy.setMode("laic");
    Xonomy.render(xml, editor, docSpec);

    function two_digits(num){
      return ("0" + num).slice(-2);
    }
    function three_digits(num){
      return ("00" + num).slice(-3);
    }
    // function reorder_counter(){
    //   var ids = [
    //     'idJabatan',
    //     'idKonsiderans',
    //     'idDasarHukum',
    //     'idMemutuskan',
    //     'idBab',
    //     'idPasal',
    //     'idBagian',
    //     'idItemPasal',
    //   ];

    //   var elem = document.querySelectorAll("[data-name="+value+"]");
    //   var counter = 1;

    //   $.each( ids, function( key, value ) {
    //     var elem = document.querySelectorAll("[data-name="+value+"]");
    //     var counter = 0;
    //     // console.log(value);
    //     // console.log(elem);
    //     $.each( elem, function( k, v ) {
    //       // console.log($(v).attr('data-value'));
    //       var str_temp = $(v).attr('data-value');
    //       if(str_temp != ''){
    //         var arr_str_temp = str_temp.split('-');
    //         arr_str_temp[2] = two_digits(counter++);
    //         console.log(arr_str_temp.join('-'));
    //         $(v).attr('data-value',arr_str_temp.join('-'));
    //         $(v).children('.valueContainer').children('.value').text(arr_str_temp.join('-'));
    //       }
          
    //     })
    //   });
    // }
    function tambah_bab(){
      action: Xonomy.newElementAfter('');
    }
    function update_idJudul (){
      idJudul = jenis + three_digits(nomor) + tahun;
      update_all_id();
    }

    function recursive_update(obj_ids,parent_id = null,node_order = 0){
      // console.log("node order : "+node_order+"-- parent_id : "+parent_id);
      $.each( obj_ids, function( key, value ) {
        // alert(typeof value);
        if(typeof value == 'string'){
          var elem = document.querySelectorAll("[data-name="+value+"]");
          
        } else if(typeof value == 'object'){
          var elem = document.querySelectorAll("[data-name="+key+"]");
          
          // recursive_update(value,node_order+1);
        }
        // 
          // var counter = 1;
          // jumlah tergantun banyaknya sub item
          var counter = [1,1,1,1,1]
          var counter_pasal =1;

          var temp_parent_counter = '';
        // 
        // console.log(elem.length)
        if(elem.length == 0){
          if(typeof value == 'object'){
            // console.log("recursive_update "+(node_order))
            recursive_update(value,parent_id,node_order+1);
          }
        } else {
          $.each( elem, function( k, v ) {

            // console.log("data-value ====== " +$(v).attr('data-value'));
            var str_temp = $(v).attr('data-value');
            // alert($(v).attr('id'))
            if(str_temp != ''){
              var arr_str_temp = str_temp.split('-');
              arr_str_temp[1] = idJudul;

              // reset counter
              


              
              // console.log("parent : "+parent_id+" - node_order :"+node_order)
              if(node_order >0 && parent_id!=null){
                // arr_str_temp[node_order+2] = two_digits(counter++);
                // console.log("parent : "+parent_id+" - ")

                var jsElement=Xonomy.harvestElement(
                    document.getElementById(Xonomy.harvestElement(
                        document.getElementById($(v).attr('id'))
                    ).internalParent.htmlID));


                // console.log(Xonomy.harvestElement(document.getElementById($(v).attr('id'))));
                var id_abal = Xonomy.harvestElement(document.getElementById($(v).attr('id'))).internalParent.htmlID;
                // console.log("haha ")
                // console.log($("#"+id_abal).parent().closest(".element").find('[data-name="'+parent_id+'"]').attr('data-value'))


                var parent_id_value = $("#"+id_abal).parent().closest(".element").find('[data-name="'+parent_id+'"]').attr('data-value');
                // var parent_id_value = parent_id_value;
                // var parent_id_value = jsElement.internalParent.getAttributeValue(parent_id);
                // console.log('parent_id_value : '+parent_id_value);
                if(parent_id_value != undefined){
                  // console.log(parent_id_value);
                  var splitted_parent_id_value = parent_id_value.split('-');
                  
                  
                  // console.log("arr_str_temp.length :"+arr_str_temp.length)

                  var length_temp = arr_str_temp.length -1;

                  for (var i = 0; i <= (length_temp - (node_order-1)) ; i++) {

                    // console.log((length_temp - (node_order-1))+" tes "+i+" - "+(arr_str_temp.length-1)+" - "+ arr_str_temp[i+2]);

                    if(splitted_parent_id_value[i+2] != undefined){
                      arr_str_temp[i+2] = two_digits(splitted_parent_id_value[i+2]);
                    } else {
                      if(arr_str_temp[i+2] != undefined){
                        arr_str_temp[i+2] = "00";
                      }
                      // if(arr_str_temp[i+2] == '00'){
                        // arr_str_temp[i+2] = "00"  ;
                      // } else {
                        // arr_str_temp[i+2] = "00"  ;
                      // } 
                    }
                    // console.log(arr_str_temp[i+2]);

                    // 
                    // arr_str_temp[i+2] = two_digits(splitted_parent_id_value[i+2]);
                  }
                }
                
              }
              // console.log(temp_parent_counter+"  = "+arr_str_temp[node_order+1])
              if(temp_parent_counter != ''){

                if(temp_parent_counter != arr_str_temp[node_order+1] ){
                  // console.log("reset")
                  counter[node_order] = 1;
                }
              }

              // console.log(" === apa ini == "+key)
              if(key == 'idPasal'){
                arr_str_temp[node_order+2] = two_digits(counter_pasal++);
              }else{

                if(key == 'idItemPasal'){
                  bentuk_item = $(v).siblings("[data-name='bentuk']").attr('data-value');
                  // console.log($bentuk_item+"  lllllllllllllllllllllllllll");
                  if(bentuk_item == "item"){
                    arr_str_temp[node_order+2] = two_digits(counter[node_order]-1);
                    arr_str_temp[node_order+1+2] = two_digits(counter[node_order+1]++);
                  } else {
                    counter[node_order+1] =1;
                    arr_str_temp[node_order+2] = two_digits(counter[node_order]++);
                  }
                } else {
                  arr_str_temp[node_order+2] = two_digits(counter[node_order]++);
                }
                
              }
              
              


              // console.log(arr_str_temp.join('-'));
              $(v).attr('data-value',arr_str_temp.join('-'));
              $(v).children('.valueContainer').children('.value').text(arr_str_temp.join('-'));
              if(typeof value == 'object'){
                recursive_update(value,key,node_order+1);
              }
            }
            // reset counter

            
            temp_parent_counter = arr_str_temp[node_order+1];
          })
        }
      });
    }
    function update_all_id (){


      recursive_update(ids2);

      var ids = [
        [
          'idJabatan',
          'idKonsiderans',
          'idDasarHukum',
          'idMemutuskan',
          'idBab',
        ],
        [
          
          'idBagian',
        ],
        [
          'idPasal',
        ],
        [
          'idItemPasal',
        ]
      ];

      var elem_idJudul = document.querySelector("[data-name='idJudul']");
      $(elem_idJudul).attr('data-value',idJudul);
      $(elem_idJudul).children('.valueContainer').children('.value').text(idJudul);

      // Xonomy.clearCache();
      // Xonomy.refresh();
      // Xonomy.validate();
      
    }

    function new_attr_xonomy(htmlID, parameter){
      // alert(parameter.name);
      // docSpec.elements['konsiderans'].attributes[parameter.name] = {
      //               asker: Xonomy.askString
      //             },
      // Xonomy.updateSpec(docSpec);
      if(/^link/.test(parameter.name)){
        counter_link++;
        // alert(counter_link);
        var splitted_str =  parameter.name.split('_');
        splitted_str[1]=counter_link;
        parameter.name = splitted_str.join('_');  
        
      }
      Xonomy.newAttribute(htmlID, parameter);
    }
    $(document).on('click','#btn-xml',function(e){
      e.preventDefault();
      var xml=Xonomy.harvest();
      var id=$('#id').val();
      // alert(xml);
      $.ajax({
        type: "POST",
        url: "{{route('builder_post')}}", 
        data: {xml:xml,id:id},
        success: function(result){
          alert(result.msg);
          if(result.id != undefined){
            $('#id').val(result.id);
          }
          
        },
        error: function(result){
          alert("penyimpanan gagal, coba lagi beberapa saat ");
        }
      });
    })
  })
</script>
@endpush