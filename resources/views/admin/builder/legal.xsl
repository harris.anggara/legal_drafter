<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">



<xsl:template match="/">
<style>
  @font-face {
      font-family: bookos;
      src: url('public/fonts/BOOKOS.TTF');
  }
  @page {
    font-family: bookos;
    <!-- margin: 120px 25px;  -->
    line-height: 2;

    font-size: 12;
    margin-left: 3cm;
    margin-top: 5cm;
    margin-right: 2.5cm;
    margin-bottom: 2.5cm;

    
    }

  td{
    text-align: justify;
    <!-- text-justify: inter-word; -->
  }

  header { position: fixed; top: -70px; left: 0px; right: 0px; background-color: white; height: 150px; }
  footer { position: fixed; bottom: -60px; left: 0px; right: 0px; background-color: white; height: 50px; }
  .page-number:after {
    content: counter(page);
  }
  .page-break {
      page-break-after: always;
  }
  h1, h2, h3, h4, h5 {
    font-weight: normal
  }
  .top-cell{
    vertical-align:top;
  }
  table{
  width:100%
  }

  .row {
  }
  .row span {
  display :   inline-block;
  vertical-align : top;
  }
  .poin {
    width:20%
  }
  .titik-dua {
    width:3%
  }
  .isi {
  width:77%;
  text-align: justify;
  }
  .nomor{
  width:5%
  }
  .isi_nomor {
  text-align: justify;
  width:72%;
  }
  .isi_sub_nomor {
  text-align: justify;
  width:67%;
  }

</style>
<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
<xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'" />

<html> 
<body>
  <!-- <header>
    <div style="text-align:center"><img height="100px" src="img/pp_bw_small.png"></img></div>
    <br></br>
    <div style="text-align:center">
      -<span class="page-number"></span>-
    </div>
    
  </header>
  <footer></footer> -->
  
  <div style="text-align:center">
    <h4><xsl:value-of select="translate(peraturan/judul/@jenis,$lowercase, $uppercase)"/> REPUBLIK INDONESIA</h4>
    <h4>NOMOR <xsl:value-of select="peraturan/judul/@nomor"/> TAHUN <xsl:value-of select="peraturan/judul/@tahun"/></h4>
    <h4>TENTANG</h4>
    <h4><xsl:value-of select="translate(peraturan/judul,$lowercase, $uppercase)"/></h4>
    <h4><xsl:value-of select="translate(peraturan/frasa,$lowercase, $uppercase)"/></h4>
    <h4><xsl:value-of select="translate(peraturan/jabatanPembentuk,$lowercase, $uppercase)"/></h4>
  </div>
  <!-- <table border="0">
    <tr>
      <td class="top-cell" width="13%">Menimbang</td>
      <td class="top-cell" width="2%">:</td>
      <td colspan="2"><xsl:value-of select="peraturan/konsiderans"/></td>
    </tr>
  </table> -->
  <xsl:for-each select="peraturan/konsiderans">
    <xsl:if test="count(../konsiderans) > 1">
      <div class="row">
        <span class="poin">
          <xsl:if test="position()=1">
              Menimbang
          </xsl:if>
        </span>
        <span class="titik-dua">
          <xsl:if test="position()=1">
              :
          </xsl:if>
        </span>
        <span class="nomor"><xsl:number value="position()" format="a"/>.</span>
        <span class="isi_nomor"><xsl:value-of select="."/></span>
      </div>
    </xsl:if>
    <xsl:if test="count(../konsiderans) = 1">
      <div class="row">
        <span class="poin">
          <xsl:if test="position()=1">
              Menimbang
          </xsl:if>
        </span>
        <span class="titik-dua">
          <xsl:if test="position()=1">
              :
          </xsl:if>
        </span>
        <span class="isi"><xsl:value-of select="."/></span>
      </div>
    </xsl:if>


    

  </xsl:for-each>
  
  <!-- <table border="1">
    <xsl:for-each select="peraturan/dasarHukum">
      <tr>
        <td  class="top-cell">
          <xsl:if test="position()=1">
            Mengingat
          </xsl:if>
        </td>
        <td class="top-cell"><xsl:if test="position()=1">:</xsl:if></td>
        <td  class="top-cell"><xsl:value-of select="position()" />.</td>
        <td><xsl:value-of select="."/></td>
      </tr>
    </xsl:for-each>
  </table> -->
  <xsl:for-each select="peraturan/dasarHukum">
    <div class="row">
      <span class="poin">
        <xsl:if test="position()=1">
          Mengingat
        </xsl:if>
      </span>
      <span class="titik-dua"><xsl:if test="position()=1">:</xsl:if></span>
      <span class="nomor"><xsl:value-of select="position()" />.</span>
      <span class="isi_nomor"><xsl:value-of select="."/></span>
    </div>
  </xsl:for-each>

  <div class="row" style="text-align : center">MEMUTUSKAN</div>
  <!-- <table border="1">
    <tr>
      <td colspan="4" style="text-align : center">MEMUTUSKAN</td>
    </tr>
  </table> -->
  <!-- <table border="1">
    <tr>
      <td class="top-cell" width="15%">Menetapkan</td>
      <td class="top-cell" width="15%">:</td>
      <td colspan="2"><xsl:value-of select="translate(peraturan/memutuskan,$lowercase, $uppercase)"/></td>
    </tr>
  </table> -->

  <div class="row">
    <span class="poin">
      Menetapkan
    </span>
    <span class="titik-dua">:</span>
    <span class="isi">
      <xsl:value-of select="translate(peraturan/memutuskan,$lowercase, $uppercase)"/>
    </span>
  </div>
  

  <xsl:for-each select="peraturan/bab">
    <div class="page-break" ></div>
    <!-- <div style="height:100px"></div> -->
    <h3 style="margin-top:100px; text-align:center">BAB <xsl:number value="position()" format="I"/></h3>
    <h3 style="text-align:center"><xsl:value-of select="@nama"/></h3>
    <xsl:for-each select="bagian">
      <h4 style="text-align:center">
        Bagian <xsl:value-of select="@nomor"/>
      </h4>
      <h4 style="text-align:center">
        <xsl:value-of select="@nama"/>
      </h4>
      <xsl:for-each select="pasal">
        <h4 style="text-align:center">
          Pasal <xsl:value-of select="format-number(substring(@idPasal,string-length(@idPasal)-1,2), '#')"/>

        </h4>

          <xsl:for-each select="itemPasal">
            
              <xsl:if test="@bentuk = 'normal'">
                <div class="row">
                  <span class="poin">
                  </span>
                  <span class="titik-dua"></span>

                  <xsl:if test="count(../itemPasal[@bentuk='normal']) = 1">

                    <span class="isi">
                      <xsl:value-of select="."/>
                    </span>

                  </xsl:if>

                  <xsl:if test="count(../itemPasal[@bentuk='normal']) > 1">

                    <span class="nomor">
                      (<xsl:value-of select="position()"/>)
                    </span>

                    <span class="isi_nomor">
                      <xsl:value-of select="."/>
                    </span>

                  </xsl:if>
                </div>
              </xsl:if>
              <xsl:if test="@bentuk = 'item'">
                <div class="row">
                  <span class="poin">
                  </span>
                  <span class="titik-dua"></span>
                  <span class="nomor">

                    </span>
                  <span class="nomor">
                    <xsl:if test="(count(preceding-sibling::*[@bentuk='normal']) > 1)">

                      <xsl:variable name="itemPosition" select="count(preceding-sibling::*[@bentuk='item'])" />
                      <xsl:variable name="normalPosition" select="count(preceding-sibling::*[@bentuk='normal'])" />

                      <xsl:if test="(preceding-sibling::*[1]/@bentuk = 'normal')">
                        <xsl:number value="(position()-$itemPosition)-$normalPosition" format="a"/>.
                      </xsl:if>

                      

                      <xsl:if test="(preceding-sibling::*[1]/@bentuk = 'item')">
                        <xsl:number value="($itemPosition - ($normalPosition + ($normalPosition - 2))+1) - ($normalPosition - 2)" format="a"/>.
                      <!-- <xsl:number value="$itemPosition"/>
                      <xsl:number value="$normalPosition"/> -->
                        <!-- <xsl:number value="(( $itemPosition - ( $normalPosition + ($normalPosition - 2) ) ) - 4 )-($normalPosition - 2)"/> -->
                      </xsl:if>
                      

                    </xsl:if>
                    <xsl:if test="not(count(preceding-sibling::*[@bentuk='normal']) > 1)">
                        <xsl:number value="position()-1" format="a" />.
                      </xsl:if>
                  </span>
                  <span class="isi_nomor">
                    <xsl:value-of select="."/>
                  </span>
                </div>
              </xsl:if>
            
          </xsl:for-each>
      
      </xsl:for-each>
    </xsl:for-each>

    <xsl:for-each select="pasal">
      <h4 style="text-align:center">
        Pasal <xsl:value-of select="format-number(substring(@idPasal,string-length(@idPasal)-1,2), '#')"/>
      </h4>
        <xsl:variable name="parent-position" select="position()" />
        <xsl:for-each select="itemPasal">

            <xsl:if test="((../../@nama = 'Ketentuan Umum') and ($parent-position=1))">

              
              <xsl:if test="@bentuk = 'normal'">
                
                <div class="row">
                    <span class="poin">
                    </span>
                    <span class="titik-dua"></span>
                    <xsl:if test="not(count(../itemPasal[@bentuk='normal']) > 1)">

                      <span class="isi">
                        <xsl:value-of select="."/>
                      </span>

                    </xsl:if>
                    <xsl:if test="count(../itemPasal[@bentuk='normal']) > 1">

                      <span class="nomor">
                        (<xsl:value-of select="count(preceding-sibling::*[@bentuk='normal'])+1"/>)
                      </span>

                      <span class="isi_nomor">
                        <xsl:value-of select="."/>
                      </span>

                    </xsl:if>
                  </div>
              </xsl:if>

              <xsl:if test="@bentuk = 'item'">

                <div class="row">
                    <span class="poin">
                    </span>
                    <span class="titik-dua"></span>
                    
                    <xsl:if test="not(count(../itemPasal[@bentuk='normal']) > 1)">

                    <span class="nomor">
                      <xsl:number value="position()-count(preceding-sibling::*[@bentuk='normal'])"/>.
                    </span>
                    <span class="isi_nomor">
                      <xsl:value-of select="."/>
                    </span>

                    </xsl:if>
                    <xsl:if test="count(../itemPasal[@bentuk='normal']) > 1">

                    <span class="nomor">

                    </span>
                    <span class="nomor">
                      <xsl:number value="position()-1" format="a"/>.
                    </span>
                    <span class="isi_sub_nomor">
                      <xsl:value-of select="."/>
                    </span>

                    </xsl:if>
                  </div>
              </xsl:if>
            </xsl:if>

            <xsl:if test="not(../../@nama = 'Ketentuan Umum') or not($parent-position=1)">
              <xsl:if test="@bentuk = 'normal'">
                <div class="row">
                    <span class="poin">
                    </span>
                    <span class="titik-dua"></span>
                    <xsl:if test="count(../itemPasal[@bentuk='normal']) = 1">

                      <span class="isi">
                        <xsl:value-of select="."/>
                      </span>

                    </xsl:if>
                    <xsl:if test="count(../itemPasal[@bentuk='normal']) > 1">

                      <span class="nomor">
                        (<xsl:value-of select="count(preceding-sibling::*[@bentuk='normal'])+1"/>)
                      </span>

                      <span class="isi_nomor">
                        <xsl:value-of select="."/>
                      </span>

                    </xsl:if>
                  </div>
              </xsl:if>

              <xsl:if test="@bentuk = 'item'">
                <!-- <xsl:value-of select="preceding-sibling::*[1]/@bentuk"/> -->
                <div class="row">
                    <span class="poin">
                    </span>
                    <span class="titik-dua"></span>
                    
                    <xsl:if test="count(../itemPasal[@bentuk='normal']) = 1">

                      <span class="nomor">
                        <xsl:number value="position() - count(preceding-sibling::*[@bentuk='normal'])" format="a"/>.
                      </span>
                      <span class="isi_nomor">
                        <xsl:value-of select="."/>
                      </span>

                    </xsl:if>
                    <xsl:if test="count(../itemPasal[@bentuk='normal']) > 1">

                      <span class="nomor">

                      </span>
                      <span class="nomor">
                        <!-- <xsl:variable name="item-position" select="count(../itemPasal[@bentuk='normal'])" /> -->
                        <!-- <xsl:value-of select="preceding-sibling::*[1]/@bentuk"/> -->
                        <xsl:if test="(count(preceding-sibling::*[@bentuk='normal']) > 1)">
                          <xsl:variable name="itemPosition" select="count(preceding-sibling::*[@bentuk='item'])" />
                          <xsl:variable name="normalPosition" select="count(preceding-sibling::*[@bentuk='normal'])" />
                          <xsl:if test="(preceding-sibling::*[1]/@bentuk = 'normal')">
                            <xsl:number value="(position()-$itemPosition)-$normalPosition" format="a"/>.
                          </xsl:if>

                          <xsl:if test="(preceding-sibling::*[1]/@bentuk = 'item')">

                            <xsl:number value="$itemPosition"/>.
                            <xsl:number value="$nomorPosition"/>.
                            <xsl:number value="(((($itemPosition - ($normalPosition + ($normalPosition - 2)))-4)-($normalPosition - 2)))" format="a"/>.
                          </xsl:if>
                          

                        </xsl:if>

                        <xsl:if test="not(count(preceding-sibling::*[@bentuk='normal']) > 1)">
                          kol <xsl:number value="position()-1" format="a"/>.
                        </xsl:if>
                        <!-- <xsl:number value="$item-position"/> -->
                        <!-- <xsl:variable name="item-position"> -->
                        <!-- <xsl:value-of select="$item-position"/> -->
                        <!-- </xsl:variable> -->
                        
                      </span>
                      <span class="isi_sub_nomor">
                        <xsl:value-of select="."/>
                      </span>

                    </xsl:if>
                  </div>
              </xsl:if>
            </xsl:if>
          
        </xsl:for-each>
      
    </xsl:for-each>
  </xsl:for-each>

</body>
</html>
</xsl:template>
</xsl:stylesheet>