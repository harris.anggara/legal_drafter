@extends('layouts.lte-app')
@push('styles')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/jquery.dataTables_themeroller.css')}}">
<!-- <link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/jquery.dataTables.min.css')}}"> -->
@endpush
@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Hover Data Table</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="list-draft" class="table table-bordered table-hover">
          <tr>
            <thead>
              <td>Id Judul</td>
              <td>Judul</td>
              <td>Keterangan</td>
              <td>Tanggal Dibuat</td>
              <td></td>
            </thead>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script type="text/javascript">
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $(document).ready(function() {
      table = $('#list-draft').DataTable({
          processing: true,
          serverSide: true,
          ajax: "{{route('dt_list_draft')}}",
          columns:[
                  {"data": 'id_judul', "name":'id_judul'},
                  {"data": 'nama_judul', "name":'nama_judul'},
                  {"data": 'keterangan', "name":'keterangan'},
                  {"data": 'created_at', "name":'created_at'},
                  {"data": 'action', "name":'action'},
                ],
          order: [ [3, 'desc'] ]
      });

      $(document).on('click','.btn-hapus',function(){
        var id = $(this).val();
        if (confirm("Yakin akan menghapus ?")) {
            $.ajax({
              type: "POST",
              url: "{{route('hapus')}}", 
              data: {id:id},
              success: function(result){
                alert(result.msg);
                table.draw();
              },
              error: function(result){
                alert("penghapusan gagal, coba lagi beberapa saat ");
              }
            });
        } else {
            // txt = "You pressed Cancel!";
        }
        
      })
  });

  function nextChar(c) {
    return String.fromCharCode(c.charCodeAt(0) + 1);
  }
</script>
@endpush