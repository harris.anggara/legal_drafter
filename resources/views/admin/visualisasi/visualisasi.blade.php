@extends('layouts.lte-app')

@push('before-styles')

  <!-- <link rel="stylesheet" href="{{asset('plugins/xonomy/xonomy.css')}}"> -->

  <style type="text/css">
    #container-sigma {
    width: inherit;
    height: 500px;
    margin: auto;
  }
  </style>
@endpush
@section('content')
<div class="box box-info">
  <div class="box-header with-border">
    <i class="fa fa-file"></i>

    <h3 class="box-title">Visualisasi</h3>
    <!-- tools box -->
    
  <!-- /. tools -->
  </div>
  <div class="box-body">
    <div id="container-sigma"></div>
      
      
  </div>
  <div class="box-footer clearfix">
  </div>
</div>



@endsection


@push('scripts')
<script type="text/javascript" src="{{asset('plugins/sigmajs/sigma.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/sigmajs/plugins/sigma.parsers.json.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/sigmajs/plugins/sigma.layout.forceAtlas2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/sigmajs/plugins/sigma.plugins.dragNodes.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    

    var s = sigma.parsers.json('data_json', {
              container: 'container-sigma',
              settings: {
                defaultNodeColor: '#ec5148'
              },
              
            },
            function(s) {
                s.graph.nodes().forEach(function(node, i, a) {
                node.x = Math.cos(Math.PI * 2 * i / a.length);
                node.y = Math.sin(Math.PI * 2 * i / a.length);
                node.size=8;
                node.color='#f00';
                });
                s.refresh();
                // s.startForceAtlas2({worker: true,adjustSizes:true,gravity: 5,outboundAttractionDistribution:true});

                var dragListener = sigma.plugins.dragNodes(s, s.renderers[0]);

                dragListener.bind('startdrag', function(event) {
                  console.log(event);
                });
                dragListener.bind('drag', function(event) {
                  console.log(event);
                });
                dragListener.bind('drop', function(event) {
                  console.log(event);
                });
                dragListener.bind('dragend', function(event) {
                  console.log(event);
                });
                // This function will be executed when the
                // graph is displayed, with "s" the related
                // sigma instance.
              }

              // Initialize the dragNodes plugin:
              
          );

   
    
  })
</script>
@endpush