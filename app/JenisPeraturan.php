<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class jenisPeraturan extends Eloquent {

    protected $collection = "daftar_peraturan";
    protected $guarded = array();
}