<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use App\Draft;
// use MarkLogic\MLPHP;
use App\Http\Controllers\Session_Basex;

class KomparasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
      return view('admin/komparasi/komparasi');
    }

    public function select_draft(Request $request){
        $uk = Draft::where('id_judul','like', '%'.$request->term.'%')->orWhere('nama_judul','like', '%'.$request->term.'%')->get();
        // dd($uk);
        $temp_arr = [];

        foreach ($uk as $key => $value) {
            array_push($temp_arr, ['id'=>$value->nama_xml,'text'=> "(".$value->id_judul.") ".$value->nama_judul]);
        }

        return response()->json([
            
                "results"=> $temp_arr,
                "pagination" => [
                    "more" => true
                ]
            
        ]);
    }

    public function cek_konflik(Request $request){
      
      if(isset($request->val_1) && isset($request->val_2)){
        $val_1 = $request->val_1;
        $val_2 = $request->val_2;

        $rs =exec('python ../python.py '.$val_1.' '.$val_2.'');
        if($rs == 'true'){
          return response()->json([
            "code" => 1,
            "msg" => "Tidak ditemukan adanya konflik"
          ]);
        } else if( $rs == 'false'){
          return response()->json([
            "code" => 0,
            "msg" => "Ditemukan adanya konflik"
          ]);
        } else {
          return response()->json([
            "code" => 2,
            "msg" => "Terjadi kesalahan; ".$rs
          ]);
        }
      // echo exec('cat ../python.py');
      } else {
        return response()->json([
          "code" => 3,
          "msg" => "tidak ada data yang diinput"
        ]);
      }
      
    }
}
