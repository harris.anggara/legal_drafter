<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Draft;
use PDF;
use Datatables;
use SimpleXMLElement;
use DOMDocument;
use XSLTProcessor;
use File;
use App\Http\Controllers\Session_Basex;

class VisualisasiController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        return view('admin/visualisasi/visualisasi');
    }


    public function data_json(){
        $temp_arr['nodes'] =[];
        $temp_arr['edges'] =[];
        $draft = Draft::get();

        $temp['nodes'] =[];
        $temp['edges'] =[];
        foreach ($draft as $key => $value) {
            
            $session = new Session_Basex(env('BASEX_HOST', 'localhost'), env('BASEX_PORT', '1984'), env('BASEX_USERNAME', 'admin'), env('BASEX_PASSWORD', 'admin'));

            // $session->execute('xquery db:open("legal_draft")');
            $data['xml'] = trim(preg_replace('/\s\s+/', '', str_replace("\n", "", $session->execute("xquery db:open('legal_draft','/drafts/".$value->nama_xml.".xml')"))));

            $xml=simplexml_load_string($data['xml']) or die("Error: Cannot create object");

            $idJudul = strtolower($xml->judul['idJudul']->__toString());
            
            if(!in_array($idJudul, $temp['nodes'])){
                array_push($temp_arr['nodes'],
                [
                  "id"=> $idJudul,
                  "label"=> $idJudul,
                  // "x"=> 0,
                  // "y"=> 0,
                  // "size"=> 3
                ]);
                array_push($temp['nodes'],$idJudul);
            }

            // dd($xml);
            foreach ($xml->konsiderans as $key2 => $value2) {
              foreach ($value2->attributes() as $key => $value) {
                # code...
                
                $value = explode('-', $value);

                if(preg_match("/^link/", $key) && isset($value[1])){
                    $id_temp = strtolower($value[1]);
                    // dd($value);

                    if(!in_array($id_temp, $temp['nodes'])){
                        array_push($temp_arr['nodes'],
                        [
                          "id"=> $id_temp,
                          "label"=> "$value[1]",
                          // "x"=> 0,
                          // "y"=> 0,
                          // "size"=> 3
                        ]);
                        array_push($temp['nodes'],$id_temp);
                    }
                    

                    if(!in_array($idJudul."-".$id_temp, $temp['edges'])){
                        array_push($temp_arr['edges'],
                        [
                          "id"=> $idJudul."-".$id_temp,
                          "source"=> $idJudul,
                          "target"=> $id_temp
                        ]);
                        array_push($temp['edges'],$idJudul."-".$id_temp);
                    }
                    
                }
            }
            }
            
            

        }
        // dd($temp_arr);

        $data = json_encode($temp_arr);
        $fileName = 'visualisasi.json';
        File::put(public_path('/json/'.$fileName),$data);


         return response()->json($temp_arr);


      //   return response()->json([
      //   "nodes" => [
      //       [
      //         "id"=> "n0",
      //         "label"=> "A node",
      //         "x"=> 0,
      //         "y"=> 0,
      //         "size"=> 3
      //       ],
      //       [
      //         "id"=> "n1",
      //         "label"=> "Another node",
      //         "x"=> 3,
      //         "y"=> 1,
      //         "size"=> 2
      //       ],
      //       [
      //         "id"=> "n2",
      //         "label"=> "And a last one",
      //         "x"=> 1,
      //         "y"=> 3,
      //         "size"=> 1
      //       ]
      //   ],
      //   "edges" => [
      //       [
      //         "id"=> "e0",
      //         "source"=> "n0",
      //         "target"=> "n1"
      //       ],
      //       [
      //         "id"=> "e1",
      //         "source"=> "n1",
      //         "target"=> "n2"
      //       ],
      //       [
      //         "id"=> "e2",
      //         "source"=> "n2",
      //         "target"=> "n0"
      //       ]
      //   ]
      // ]);
    }
}
