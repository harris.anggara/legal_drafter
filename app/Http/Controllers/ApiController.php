<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Draft;
use PDF;
use Datatables;
use SimpleXMLElement;
use Response;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function return_xml($id){
      $data['draft'] = Draft::where('_id',$id)->first();


      // XML
      $session = new Session_Basex(env('BASEX_HOST', 'localhost'), env('BASEX_PORT', '1984'), env('BASEX_USERNAME', 'admin'), env('BASEX_PASSWORD', 'admin'));

      // $session->execute('xquery db:open("legal_draft")');
      $xml = trim(preg_replace('/\s\s+/', '', str_replace("\n", "", $session->execute("xquery db:open('legal_draft','/drafts/".$data['draft']->nama_xml.".xml')"))));

      $xml = new SimpleXMLElement($xml);

      // echo $xml->asXML();
      $response = Response::make($xml->asXML(), 200);
      $response->header('Content-Type', 'text/xml');
      $response->header('Cache-Control', 'public');
      $response->header('Content-Description', 'File Transfer');
      $response->header('Content-Disposition', 'attachment; filename='.$data['draft']->id_judul.'_'.$data['draft']->nama_judul.'.xml');
      $response->header('Content-Transfer-Encoding', 'binary');
      $response->header('Content-Type', 'text/xml');


      return $response;
      
      // Response::macro('xml', function(array $vars, $status = 200, array $header = [], $rootElement = 'response', $xml = null)
      // {
      //     if (is_null($xml)) {
      //         $xml = new SimpleXMLElement('<'.$rootElement.'/>');
      //     }

      //     foreach ($vars as $key => $value) {
      //         if (is_array($value)) {
      //             Response::xml($value, $status, $header, $rootElement, $xml->addChild($key));
      //         } else {
      //             if( preg_match('/^@.+/', $key) ) {
      //                 $attributeName = preg_replace('/^@/', '', $key);
      //                 $xml->addAttribute($attributeName, $value);
      //             } else {
      //                 $xml->addChild($key, $value);
      //             }
      //         }
      //     }

      //     if (empty($header)) {
      //         $header['Content-Type'] = 'application/xml';
      //     }

      //     return Response::make($xml->asXML(), $status, $header);
      // });
    }

    public function xml_draft(){
        $draft = Draft::first();

       $draft_xml = new SimpleXMLElement("<draft></draft>");

       $jenis = $draft_xml->addChild('jenis',$draft->jenis);
       $nomor = $draft_xml->addChild('nomor',$draft->nomor);
       $tahun = $draft_xml->addChild('tahun',$draft->tahun);
       $judul = $draft_xml->addChild('judul',$draft->judul);
       $frasa = $draft_xml->addChild('frasa',$draft->frasa);
       $jabatan_pembentuk = $draft_xml->addChild('jabatan_pembentuk',$draft->jabatan_pembentuk);

       $konsiderans = $draft_xml->addChild('konsiderans');

       foreach ($draft->konsiderans as $k => $v) {
           $item[$k] = $konsiderans->addChild('item');
       }

       $dasar_hukum = $draft_xml->addChild('dasar_hukum');
       
       foreach ($draft->dasar_hukum as $k => $v) {
           $item[$k] = $dasar_hukum->addChild('item');
       }

       $memutuskan = $draft_xml->addChild('memutuskan');

       $ketentuan_umum = $draft_xml->addChild('ketentuan_umum');
       
       foreach ($draft->ketentuan_umum as $k => $v) {
           $item[$k] = $ketentuan_umum->addChild('item');
       }

       $ketentuan_pidana = $draft_xml->addChild('ketentuan_pidana');
       
       foreach ($draft->ketentuan_pidana as $k => $v) {
           $item[$k] = $ketentuan_pidana->addChild('item');
       }

       $ketentuan_penutup = $draft_xml->addChild('ketentuan_penutup');
       
       foreach ($draft->ketentuan_penutup as $k => $v) {
           $item[$k] = $ketentuan_penutup->addChild('item');
       }

       $penutup = $draft_xml->addChild('penutup');
       
       foreach ($draft->penutup as $k => $v) {
           $item[$k] = $penutup->addChild('item');
       }
       
        Header('Content-type: text/xml');
        echo $draft_xml->asXML();
    }
}
