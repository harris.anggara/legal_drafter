<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Draft;
use PDF;
use Datatables;
use SimpleXMLElement;
use DOMDocument;
use XSLTProcessor;
use Storage;
use App\JenisPeraturan;
use App\Http\Controllers\Session_Basex;

class BuilderController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    private function jenis_dokumen($str){

        $jenis_dok = JenisPeraturan::where('nama', strtolower($str))->first();

        if(count($jenis_dok)>0){
            return $jenis_dok->singkatan;
        } else {
            return 'no';
        }
        // switch (strtolower($str)) {
        //     case 'peraturan pemerintah':
        //         return 'pp';
        //         break;
            
        //     default:
        //         return 'un';
        //         break;
        // }
    }
    public function jenis_dokumen_ajax(Request $request){

        $jenis_dok = JenisPeraturan::where('nama', strtolower($request->str))->first();

        if(count($jenis_dok)>0){
            return response()->json([
                'result' => $jenis_dok->singkatan,
            ]);
        } else {
            return response()->json([
                'result' => 'no',
            ]);
        }
    }
    public function get_pasal_number($id_pasal){
        // return ltrim(end(explode('-',$id_pasal)), '0');
        return "tes";
    }
    public function build_pdf(Request $request){
        if(isset($request->id)){
            $id = $request->id;
            $data['draft'] = Draft::where('_id',$id)->first();


            // XML
            $session = new Session_Basex(env('BASEX_HOST', 'localhost'), env('BASEX_PORT', '1984'), env('BASEX_USERNAME', 'admin'), env('BASEX_PASSWORD', 'admin'));

            // $session->execute('xquery db:open("legal_draft")');
            $data['xml'] = trim(preg_replace('/\s\s+/', '', str_replace("\n", "", $session->execute("xquery db:open('legal_draft','/drafts/".$data['draft']->nama_xml.".xml')"))));
            $xml_doc = new DOMDocument();
            $xml_doc->loadXML($data['xml']);
            // print_r($xml_doc);



            // XSL
            $xsl_doc = new DOMDocument();
            $xsl_doc->load('../resources/views/admin/builder/legal.xsl');


            // Proc
            $proc = new XSLTProcessor();
            $proc->setProfiling('../public/profiling_xslt/profiling.txt');
            $proc->registerPHPFunctions();
            $proc->importStylesheet($xsl_doc);
            $newdom = $proc->transformToDoc($xml_doc);
            // $newdom = $proc->transformToDoc($xml_doc);

            // print($data['xml']);
            // print $newdom->saveXML();

            $pdf = PDF::loadHTML($newdom->saveXML())->setPaper(array(0,0,612,972), 'portrait');

            return $pdf->stream('tes.pdf');
        } else {
            return "tidak ada id";
        }
    }
    public function index(Request $request)
    {
        if(isset($request->id)){
            $id = $request->id;
            $data['draft'] = Draft::where('_id',$id)->first();


            $session = new Session_Basex(env('BASEX_HOST', 'localhost'), env('BASEX_PORT', '1984'), env('BASEX_USERNAME', 'admin'), env('BASEX_PASSWORD', 'admin'));

            // $session->execute('xquery db:open("legal_draft")');
            $data['xml'] = trim(preg_replace('/\s\s+/', '', str_replace("\n", "", $session->execute("xquery db:open('legal_draft','/drafts/".$data['draft']->nama_xml.".xml')"))));

            $xml=simplexml_load_string($data['xml']) or die("Error: Cannot create object");

            // nyari jumlah link id
            $ct_link = 0;
            $data['arr_link']=[];

            foreach($xml->xpath('konsiderans') as $k => $v)
            {
                // dd($v);
                foreach ($v->attributes() as $k2 => $v2) {
                    if(preg_match("/^link/", $k2)){
                        $ct_link = (int)explode('_', $k2)[1];
                        // $ct_link++;
                        array_push($data['arr_link'], $k2);
                    }
                }
              
            }
            // foreach ($xml->konsiderans->attributes() as $key => $value) {
            //     # code...
                
            // }


            $data['total_link'] = $ct_link;
            $data['idJudul'] = $xml->judul['idJudul'];
            $data['jenis'] = $this->jenis_dokumen($xml->judul['jenis']);
            $data['nomor'] = $xml->judul['nomor'];
            $data['tahun'] = $xml->judul['tahun'];

        } else {
            $id = '';
            $data['draft'] = null;
            $data['xml'] = '<peraturan xsi:noNamespaceSchemaLocation="schema.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'.
                       '<judul idJudul="" kodifikasi="none" kompilasi="none" status="baru" jenis="" nomor="" tahun="">'.
                            // '<jenis></jenis>'.
                            // '<nomor></nomor>'.
                            // '<tahun></tahun>'.
                            // '<namaJudul></namaJudul>'.
                        '</judul>'.
                        '<frasa>Dengan Rahmat Tuhan Yang Maha Esa</frasa>'.
                        '<jabatanPembentuk idJabatan="j-id"></jabatanPembentuk>'.
                        '<konsiderans idKonsiderans="k-id-01"></konsiderans>'.
                        '<dasarHukum idDasarHukum="dh-id-01"></dasarHukum>'.
                        // '<dasarHukum idDasarHukum="dh-PP082012-02"></dasarHukum>'.
                        '<memutuskan idMemutuskan="m-id"></memutuskan>'.
                        '<bab idBab="b-id-1" nama="Ketentuan Umum">'.
                        '</bab>'.
                    '</peraturan>';
            $data['arr_link']=[];
            $data['total_link'] = 0;
            $data['idJudul'] = '';
            $data['jenis'] = '';
            $data['nomor'] = '';
            $data['tahun'] = '';
        }
        // print($data['xml']);
        // exit;
        $data['id'] = $id;
        return view('admin/builder/builder2', $data);
    }

    public function anotator(Request $request)
    {
        $default_xml = '<peraturan xsi:noNamespaceSchemaLocation="schema.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'.
                       '<judul idJudul="" kodifikasi="none" kompilasi="none" status="baru" jenis="" nomor="" tahun="">'.
                            // '<jenis></jenis>'.
                            // '<nomor></nomor>'.
                            // '<tahun></tahun>'.
                            // '<namaJudul></namaJudul>'.
                        '</judul>'.
                        '<frasa>Dengan Rahmat Tuhan Yang Maha Esa</frasa>'.
                        '<jabatanPembentuk idJabatan="j-id"></jabatanPembentuk>'.
                        '<konsiderans idKonsiderans="k-id-01"></konsiderans>'.
                        '<dasarHukum idDasarHukum="dh-id-01"></dasarHukum>'.
                        // '<dasarHukum idDasarHukum="dh-PP082012-02"></dasarHukum>'.
                        '<memutuskan idMemutuskan="m-id"></memutuskan>'.
                        '<bab idBab="b-id-1" nama="Ketentuan Umum">'.
                        '</bab>'.
                    '</peraturan>';

        if(isset($request->id)){
            $id = $request->id;
            $data['draft'] = Draft::where('_id',$id)->first();

            if($data['draft']->nama_xml != ''){
                $session = new Session_Basex(env('BASEX_HOST', 'localhost'), env('BASEX_PORT', '1984'), env('BASEX_USERNAME', 'admin'), env('BASEX_PASSWORD', 'admin'));

                // $session->execute('xquery db:open("legal_draft")');
                $data['xml'] = trim(preg_replace('/\s\s+/', '', str_replace("\n", "", $session->execute("xquery db:open('legal_draft','/drafts/".$data['draft']->nama_xml.".xml')"))));
            } else {
                $data['xml'] = $default_xml;
            }
            

            $xml=simplexml_load_string($data['xml']) or die("Error: Cannot create object");

            // dd($data['xml']);
            // nyari jumlah link id
            $ct_link = 0;
            $data['arr_link']=[];

            foreach($xml->xpath('konsiderans') as $k => $v)
            {
                // dd($v);
                foreach ($v->attributes() as $k2 => $v2) {
                    if(preg_match("/^link/", $k2)){
                        $ct_link = (int)explode('_', $k2)[1];
                        // $ct_link++;
                        array_push($data['arr_link'], $k2);
                    }
                }
              
            }
            // foreach ($xml->konsiderans->attributes() as $key => $value) {
            //     # code...
                
            // }


            $data['total_link'] = $ct_link;
            $data['idJudul'] = $xml->judul['idJudul'];
            $data['jenis'] = $this->jenis_dokumen($xml->judul['jenis']);
            $data['nomor'] = $xml->judul['nomor'];
            $data['tahun'] = $xml->judul['tahun'];

        } else {
            $id = '';
            $data['draft'] = null;
            $data['xml'] = $default_xml;
            $data['arr_link']=[];
            $data['total_link'] = 0;
            $data['idJudul'] = '';
            $data['jenis'] = '';
            $data['nomor'] = '';
            $data['tahun'] = '';
        }
        // print($data['xml']);
        // exit;
        $data['id'] = $id;

        // sementara ya
        // $pdf_link = Storage::get('file.jpg')
        return view('admin/builder/anotator', $data);
    }

    public function upload_pdf(Request $request){
        $this->validate($request, [
            'pdf' => 'mimes:pdf'
        ]);

        // dd( );
        if($request->id == null){
            $filename = $request->user()->id."_".date("Y-m-d_H-i-s").".pdf";
            $path = $request->file('pdf')->storeAs(
                'public/pdf_sumber', $filename
            );
            $draft = new Draft;
            $draft->id_judul = '';
            $draft->nama_judul = '';
            $draft->nama_xml = '';
            $draft->pdf_sumber = $filename;
            $draft->original_filename = $request->pdf->getClientOriginalName();
            $draft->jenis = "anotasi";

            $draft->save();
        } else {
            $filename = $request->user()->id."_".date("Y-m-d_H-i-s").".pdf";
            $path = $request->file('pdf')->storeAs(
                'public/pdf_sumber', $filename
            );
            $draft = Draft::where('_id',$id)->first();
            $draft->id_judul = '';
            $draft->nama_judul = '';
            $draft->nama_xml = '';
            $draft->pdf_sumber = $filename;
            $draft->original_filename = $request->pdf->getClientOriginalName();
            $draft->save();
        }
        

        // return $path;
        return response()->json([
                'success' => true,
                'msg' => 'berhasil terupload',
                'id' => $draft->id,
                'prev_id' => $request->id,
                'filename' => $filename
            ]);
            
    }

    public function tiny(){
        return view('admin/builder/tinymce');
    }

    public function tiny_post(Request $request){
        dd($request->draft);
    }

    public function simpan(Request $request)
    {
        
        $xml_string = $request->xml;
        // dd($request->id);
        $xml=simplexml_load_string($xml_string) or die("Error: Cannot create object");


        if($request->id == null or $request->id == '' ){
            $draft = new Draft;
            $nama_xml = $xml->judul['idJudul']."_".date("Y-d-m-H-i-s");
        } else {
            $draft = Draft::where(['_id' => $request->id])->first();
            if($draft->nama_xml == ''){
                $nama_xml = $xml->judul['idJudul']."_".date("Y-d-m-H-i-s");
            } else {
                $nama_xml = $draft->nama_xml;
            }
            
        }

        try {
            // $this->convert_to_pl($xml_string,$nama_xml);

            $session = new Session_Basex(env('BASEX_HOST', 'localhost'), env('BASEX_PORT', '1984'), env('BASEX_USERNAME', 'admin'), env('BASEX_PASSWORD', 'admin'));

            // $session->execute('xquery db:open("legal_draft")');
            $session->execute("open legal_draft");
            
            $save = $session->replace("/drafts/".$nama_xml.".xml", $xml_string);
            
            $session->close();
            $this->convert_to_pl($xml_string,$nama_xml);



            // simpen ke mongo 
            $xml_string_judul = $xml->judul['idJudul']->__toString();
            $draft->id_judul = $xml_string_judul;
            $draft->nama_judul = $xml->judul->__toString();
            // dd($xml_string_judul);
            $draft->nama_xml = $nama_xml;

            $save = $draft->save();
            // dd($save);
            // if($save){
            return response()->json([
                'success' => true,
                'msg' => 'berhasil tersimpan',
                'id' => $draft->id,
                'prev_id' => $request->id
            ]);
            
        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'msg' => 'gagal tersimpan',
                'id' => $draft->id,
                'prev_id' => $request->id
            ]);
        }
        

        

        
    }
    function hapus(Request $request){
        // dd($request->id);
        $draft = Draft::where('_id', $request->id)->first();
        // dd($draft);
        $del = $draft->delete();
        // dd($del);
        if($del){
            return response()->json([
                'success' => true,
                'msg' => 'Berhasil Dihapus',
                'id' => $draft->id,
                'prev_id' => $request->id
            ]);
        } else {
            return response()->json([
                'success' => false,
                'msg' => 'Gagal Dihapus',
                'id' => $draft->id,
                'prev_id' => $request->id
            ]);
        }

    }
    function convert_to_pl($xml_string, $filename){
	
        $xml = new SimpleXMLElement($xml_string);

        $str_temp = '';
        $jd = $this->jenis_dokumen($xml->judul['jenis']);



        $str_temp .= "idJudul('".$xml->judul->attributes()->idJudul."')\njenis('".$xml->judul->attributes()->idJudul."', ".$jd.")\nnomor('".$xml->judul->attributes()->idJudul."', ".$xml->judul['nomor'].")\ntahun('".$xml->judul->attributes()->idJudul."', ".$xml->judul['tahun'].")\nkodifikasi('".$xml->judul->attributes()->idJudul."', ".$xml->judul->attributes()->kodifikasi.")\nkompilasi('".$xml->judul->attributes()->idJudul."', ".$xml->judul->attributes()->kompilasi.")\nstatus('".$xml->judul->attributes()->idJudul."', ".$xml->judul->attributes()->status.")\nidJabatan('".$xml->judul->attributes()->idJudul."', '".$xml->jabatanPembentuk->attributes()->idJabatan."')\n";
        // dd($xml->xpath('konsiderans'));
        foreach($xml->xpath('konsiderans') as $k => $v)
        {
            // dd($v);
            foreach ($v->attributes() as $k2 => $v2) {
                # code...
                if ($k2 =='idKonsiderans') {
                    $str_temp .= "idKonsiderans('".$v2."')\n";
                }
                if(preg_match("/^link/", $k2)){
                    $str_temp .= "linkKonsiderans('".$xml->judul->attributes()->idJudul."','".$v2."')\n";
                    // array_push($data['arr_link'], $key);
                }
            }
          
        }
        foreach($xml->xpath('dasarHukum') as $k => $v)
        {
            $str_temp .= "idDasarHukum('".$v->attributes()->idDasarHukum."')\n";
        }
        foreach($xml->xpath('memutuskan') as $k => $v)
        {
            $str_temp .= "idMemutuskan('".$v->attributes()->idMemutuskan."')\n";
        }
        foreach($xml->xpath('bab') as $k => $v)
        {
            $str_temp .= "idBab('".$v->attributes()->idBab."')\n";
            $str_temp .= "namaBab('".$v->attributes()->nama."')\n";
        }
        foreach($xml->xpath('bab/bagian') as $k => $v)
        {
            $str_temp .= "idBagian('".$v->attributes()->idBagian."')\n";
            $str_temp .= "nomorBagian('".$v->attributes()->idBagian."','".$v->attributes()->nomor."')\n";
            $str_temp .= "namaBagian('".$v->attributes()->idBagian."','".$v->attributes()->nama."')\n";
        }
        foreach($xml->xpath('bab/bagian/pasal') as $k => $v)
        {
            $str_temp .= "idPasal('".$v->attributes()->idPasal."')\n";
        }
        foreach($xml->xpath('bab/pasal') as $k => $v)
        {
            $str_temp .= "idPasal('".$v->attributes()->idPasal."')\n";
        }
        foreach($xml->xpath('bab/bagian/pasal/itemPasal') as $k => $v)
        {
            $str_temp .= "idItemPasal('".$v->attributes()->idItemPasal."')\n";
        }
        foreach($xml->xpath('bab/pasal/itemPasal') as $k => $v)
        {
            $str_temp .= "idItemPasal('".$v->attributes()->idItemPasal."')\n";
        }

        $str_temp .= "memiliki('judul', [".$jd.", ".$xml->judul['nomor'].", ".$xml->judul['tahun']."])\n";

        $str_temp_2 = '';
        $i = 0;
        foreach($xml->xpath('konsiderans') as $k => $v)
        {
            // dd($v);
            if($i > 0 ){
                $str_temp_2.=",";
            }
            foreach ($v->attributes() as $k2 => $v2) {
                # code...
                if ($k2 =='idKonsiderans') {
                    $str_temp_2 .= "['".$v2."']";
                }
            }
            $i++;
        }
        $str_temp .= "memiliki(konsiderans, ".$str_temp_2.")\n";

        $str_temp_2 = '';
        $i = 0;
        foreach($xml->xpath('dasarHukum') as $k => $v)
        {
            // dd($v);
            if($i > 0 ){
                $str_temp_2.=",";
            }
            $str_temp_2 .= "['".$v2."']";
        
            $i++;
        }
        $str_temp .= "memiliki(dasarHukum, ".$str_temp_2.")\n";

        
        // $i = 0;
        foreach($xml->xpath('bab') as $k => $v)
        {
            $str_temp_2 = '';
            $str_temp_2 .= "['".$v->attributes()->idBab."'],['".$v->attributes()->nama."']";
            $str_temp .= "memiliki(bab, ".$str_temp_2.")\n";
            // $i++;
        }
        

        
        // $i = 0;
        foreach($xml->xpath('bab/bagian/pasal/itemPasal') as $k => $v)
        {
            $str_temp_2 = '';
            $str_temp_2 .= "['".$v->attributes()->idItemPasal."'],['".$v->attributes()->bentuk."']";
            $str_temp .= "memiliki(itemPasal, ".$str_temp_2.")\n";
            // $i++;
        }
        

        
        // $i = 0;
        foreach($xml->xpath('bab/pasal/itemPasal') as $k => $v)
        {
            $str_temp_2 = '';
            $str_temp_2 .= "['".$v->attributes()->idItemPasal."'],['".$v->attributes()->bentuk."']";
            $str_temp .= "memiliki(itemPasal, ".$str_temp_2.")\n";
            // $i++;
        }
        
        $str_temp_2 = '';
        
        foreach($xml->xpath('bab/bagian/pasal') as $k => $v)
        {
            $i = 0;
            $str_temp_2 = '[';
            foreach($v->children() as $k2 => $v2)
            {
                if($i > 0 ){
                    $str_temp_2.=",";
                }
                $str_temp_2 .= "'".$v2->attributes()->idItemPasal."'";
                $i++;
            }
            $str_temp_2 .= ']';
            $str_temp .= "terdiriDari(pasal, ".$str_temp_2.")\n";
            
        }
        
        $str_temp_2 = '';
        
        foreach($xml->xpath('bab/pasal') as $k => $v)
        {
            $i = 0;
            $str_temp_2 = '[';
            foreach($v->children() as $k2 => $v2)
            {
                if($i > 0 ){
                    $str_temp_2.=",";
                }
                $str_temp_2 .= "'".$v2->attributes()->idItemPasal."'";
                $i++;
            }
            $str_temp_2 .= ']';
            $str_temp .= "terdiriDari(pasal, ".$str_temp_2.")\n";
            
        }
        
        
        // foreach($xml->children() as $k => $v)
        // {
        //   $str_temp .= $k.' = '.$v."\n";
        // }
	
        Storage::disk('local')->put($filename.'.pl', $str_temp);

    }
    public function preview(Request $request)
    {
        // dd($request->input());
        $data =[
            'jenis' => $request->jenis,
            'nomor' => $request->nomor,
            'tahun' => $request->tahun,
            'judul' => $request->judul,
            'frasa' => $request->frasa,
            'jabatan_pembentuk' => $request->jabatan_pembentuk,
            'konsiderans' => $request->konsiderans,
            'dasar_hukum' => $request->dasar_hukum,
            'memutuskan' => $request->memutuskan,
            'ketentuan_umum' => $request->ketentuan_umum,
            'ketentuan_pidana' => $request->ketentuan_pidana,
            'ketentuan_penutup' => $request->ketentuan_penutup,
            'penutup' => $request->penutup,
        ];


        $data['alphabet'] = range('a', 'z');

        $pdf = PDF::loadView('admin/html/draft',$data)->setPaper(array(0,0,612,972), 'portrait');

        return $pdf->stream($data['judul'].'.pdf');
    }

    public function dt_list_draft(){
        $draft = Draft::get();
        return Datatables::of($draft)
                ->addColumn('no_tahun', function ($draft) {
                    return 'No. '.$draft->nomor.' Tahun '.$draft->tahun;
                })
                ->addColumn('keterangan', function ($draft){
                    return $draft->jenis."<br>".$draft->original_filename;
                })
                ->addColumn('action', function ($draft) {
                    if($draft->jenis == "anotasi"){
                        $btn = '<form method="post" action="'.route('anotator').'" >'.csrf_field().'<button value="'.$draft->_id.'" name="id" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</button></form>';
                    } else {
                        $btn = '<form method="post" action="'.route('builder').'" >'.csrf_field().'<button value="'.$draft->_id.'" name="id" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</button></form>';
                    }
                    $btn =  $btn.'<form method="post" action="'.route('build_pdf').'" >'.csrf_field().'<button value="'.$draft->_id.'" name="id" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-file"></i> PDF</button></form><button value="'.$draft->_id.'" name="id" class="btn btn-xs btn-danger btn-hapus"><i class="glyphicon glyphicon-trash"></i> Hapus</button>
                            </form><br>
                                <a href="'.route('return_xml',['id'=>$draft->_id]) .'" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-file-alt"></i> XML</a>
                            ';

                    return $btn;
                })
                ->rawColumns(['keterangan', 'action'])
                ->make(true);
    }

    public function list_draft(){
        return view('admin/builder/list_draft');
    }

    public function xml_draft(){
       $newsXML = new SimpleXMLElement("<news></news>");
        $newsXML->addAttribute('newsPagePrefix', 'value goes here');
        $newsIntro = $newsXML->addChild('content');
        $newsIntro->addAttribute('type', 'latest');
        Header('Content-type: text/xml');
        echo $newsXML->asXML();
    }
}
