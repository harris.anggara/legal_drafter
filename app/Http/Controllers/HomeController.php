<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
// use MarkLogic\MLPHP;
use App\Http\Controllers\Session_Basex;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/dashboard');
    }

    public function tes_xml(){
        try {
             // create session
              $session = new Session_Basex(env('BASEX_HOST', 'localhost'), env('BASEX_PORT', '1984'), env('BASEX_USERNAME', 'legal_drafter'), env('BASEX_PASSWORD', 'legal_drafter.'));
              
              // create new database
              // $session->execute("create db legal_draft");
              // print $session->info();

              $session->execute("open legal_draft");
              
              // // add document
              // $session->add("world/World.xml", "<x>Hello World!</x>");
              // print "<br/>".$session->info();
              
              // // add document
              // $session->add("Universe.xml", "<xml><x name='harris' alamat='dimana'>Harris</x><x name='tiwi'>tiwi</x></xml>");
              // print "<br/>".$session->info();
              
              // // run query on database
              // print "<br/>".$session->execute('xquery 
              //   for $x in db:open("database","Universe.xml")/xml/x
              //   where $x/@name = "harris"
              //   return $x
              //   ');
              

              $collections = 'for $x in fn:uri-collection() return $x';
              $query = $session->query($collections);
              // loop through all results
              $arr_collections = [];

                while($query->more()) {
                    array_push($arr_collections, $query->next());
                }

                dd($arr_collections);

              // dd($session->execute('xquery fn:uri-collection()'));
              // print "<br/>";
              // print "<br/>".$session->execute('xquery 
              //   for $x in fn:uri-collection()
              //   return $x
              //   ');
                
              
              // drop database
              $session->execute("drop db legal_draft");
              // close session
              $session->close();
            } catch (Exception $e) {
              // print exception
              print $e->getMessage();
            }
    }
}
